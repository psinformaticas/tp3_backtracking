package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Color;

import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JTable;

import controlador.Contenedor;
import controlador.FuncionesStrings;
import modelo.CondicionesEquipo;
import modelo.Equipo;
import modelo.ParesIncompatibles;
import modelo.Persona;
import modelo.TMCondiciones;
import modelo.TMEquipo;
import modelo.TMPersona;
import modelo.TipoSolucion;

import javax.swing.JScrollPane;
import javax.swing.border.EtchedBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.border.TitledBorder;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import modelo.Roles;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JList;
import javax.swing.UIManager;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.SystemColor;

public class Principal {

	private JFrame frame;

	//contenedor.getSistema()
	public static Contenedor contenedor;
	
	//BOTONES
	private JButton btnPersonas;
	private JButton btnEquipos;
	private JButton btnCondiciones;
	private JButton btnRelaciones;
	
	private JButton	btnAgregarPersona;
	private JButton btnModificarPersona;
	private JButton btnEliminarPersona;
	private JButton btnLimpiarPersona;

	private JButton btnAgregarCond;
	private JButton btnModificarCond;
	private JButton btnEliminarCond;
	private JButton btnLimpiarCond;
	private JButton btn_heuristica;
	private JButton btn_fuerzaBruta;
	
	//TABLEMODELS
	private TMPersona tableModelPersona;
	public static TMEquipo tableModelEquipo;
	private TMCondiciones tableModelCondiciones;
	private JTable tablaPersonas;
	public static JTable tablaEquipos;
	private JTable tablaPersonasRelaciones;
	private JTable tablaCondiciones;
	
	
	//TEXT FIELDS
	private JTextField tf_ID;
	private JTextField tf_nombre;
	private JTextArea ta_idea;
	private JTextArea ta_datosEquipo;
	private JTextField tf_nomBusqueda;
	public static JTextField tf_nomEquipoBusqueda;
	private JTextField tf_condBusqueda;
	private JTextField tf_nombreRelaciones;
	
	//LABELS
	private JLabel lbFotoPersona;
	public static JLabel lbEstadoCond;
	
	//IMAGEICONS
	private ImageIcon fotoPersona;
	
	//JFRAMES
	private VerFotoPersona windowVerFoto;
	private JTextField tf_nombre_equipo;
	
	//JLIST
	private JList listaIntegrantes;
	private JList listaIncompatibilidades;
	
	//TABBED PANE
	private JTabbedPane panelSolapas;
	private JTextField td_nombre_proyecto;
	
	
	//COMBOS
	private JComboBox<Integer> comboLideresMin;
	private JComboBox<Integer> comboLideresMax;
	private JComboBox<Integer> comboArquitectosMin;
	private JComboBox<Integer> comboArquitectosMax;
	private JComboBox<Integer> comboProgramadoresMin;
	private JComboBox<Integer> comboProgramadoresMax;
	private JComboBox<Integer> comboTestersMin;
	private JComboBox<Integer> comboTestersMax;
	
	private JComboBox<Roles> comboRol;
	
	public static JPanel panelEstadoCond;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal window = new Principal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Principal() {
		initialize();
		contenedor = new Contenedor();
		
		llenarCombos();		
		
		
		setPanelPersonaAgregar();
		setPanelEquipoEnBlanco();
		setPanelCondicionAgregar();
		
		cargaInicial(); 
	}
	

	// **************************
	// * CARGA INICIAL DE DATOS *
	// **************************
	private void cargaInicial() {
		contenedor.getSistema().cargarDatosJSON();
		cargaTablaPersonas();
		cargaTablaPersonasRelaciones();
		cargaTablaEquipos();
		cargarListaIncompatibles();
		cargaTablaCondiciones();
	}
	
	
	// *******************
	// * LISTAS Y COMBOS *
	// *******************
	
	private void cargarListaIncompatibles() {
		Object[] incompatibles=new Object[contenedor.getSistema().getListaIncompatibles().size()];
		String[] idsIncompatibles = new String[contenedor.getSistema().getListaIncompatibles().size()];
		
		int cont = 0;
		for (ParesIncompatibles par: contenedor.getSistema().getListaIncompatibles()) {
			incompatibles[cont] = FuncionesStrings.capitalize(contenedor.getSistema().getPersonaPorId(par.getId_1()).getNombre()+" - "+contenedor.getSistema().getPersonaPorId(par.getId_2()).getNombre());
			idsIncompatibles[cont] =par.getId_1()+"-"+par.getId_2(); 
			cont++;
		}
		
		listaIncompatibilidades.setListData(incompatibles);
		  
		RenderListaIncompatibles rli=new RenderListaIncompatibles(incompatibles, idsIncompatibles);
		  
		listaIncompatibilidades.setCellRenderer(rli);
	}
	
	private void llenarCombos() {
		for (int i=1; i<100; i++) {
			comboLideresMin.addItem(i);
			comboLideresMax.addItem(i);
			comboArquitectosMin.addItem(i);
			comboArquitectosMax.addItem(i);
			comboProgramadoresMin.addItem(i);
			comboProgramadoresMax.addItem(i);
			comboTestersMin.addItem(i);
			comboTestersMax.addItem(i);
		}
	}
	
	// ***********
	// * TABLAS  *
	// ***********
	
	private void cargaTablaPersonas() {
		ArrayList<Persona> personasBusqueda = new ArrayList<>();
		for (Persona personaEnBusqueda: contenedor.getSistema().getPersonas()) {
			if (personaEnBusqueda.getNombre().toLowerCase().contains(tf_nomBusqueda.getText().toLowerCase())) {
				personasBusqueda.add(personaEnBusqueda);
			}
		}
		tableModelPersona = new TMPersona(personasBusqueda);
		tablaPersonas.setModel(tableModelPersona);
	}
	
	private void cargaTablaPersonasRelaciones() {
		ArrayList<Persona> personasBusqueda = new ArrayList<>();
		for (Persona personaEnBusqueda: contenedor.getSistema().getPersonas()) {
			if (personaEnBusqueda.getNombre().toLowerCase().contains(tf_nombreRelaciones.getText().toLowerCase())) {
				personasBusqueda.add(personaEnBusqueda);
			}
		}
		tableModelPersona = new TMPersona(personasBusqueda);
		tablaPersonasRelaciones.setModel(tableModelPersona);
	}
	
	public static void cargaTablaEquipos() {
		ArrayList<Equipo> equiposBusqueda = new ArrayList<>();
		for (Equipo equipoBusqueda: contenedor.getSistema().getEquiposDeTrabajo()) {
			if (equipoBusqueda.getNombreDelProyecto().toLowerCase().contains(tf_nomEquipoBusqueda.getText().toLowerCase())) {
				equiposBusqueda.add(equipoBusqueda);
			}
		}
		
		tableModelEquipo = new TMEquipo(equiposBusqueda);
		tablaEquipos.setModel(tableModelEquipo);
	}
	

	
	private void cargaTablaCondiciones() {
		ArrayList<CondicionesEquipo> condEquipos = new ArrayList<>();
		for (CondicionesEquipo condEquipo: contenedor.getSistema().getCondicionesParaEquipos()) {
			if (condEquipo.getNombreDelProyecto().toLowerCase().contains(tf_condBusqueda.getText().toLowerCase())) {
				condEquipos.add(condEquipo);
			}
		}
		
		tableModelCondiciones = new TMCondiciones(condEquipos);
		tablaCondiciones.setModel(tableModelCondiciones);
	}
	
	private Roles getRolSeleccionadoEnPersona() {
		Roles rol = (Roles) comboRol.getSelectedItem();
		return rol;
	}
	
	// *******************
	// * SETTERS PANELES *
	// *******************
	
	private void setPanelPersonaAgregar() {
		comboRol.setSelectedItem(Roles.Lider);
		tf_ID.setText("");
		tf_ID.setEditable(false);
		tf_nombre.setText("");
		tf_nombre.setEditable(true);
		btnAgregarPersona.setEnabled(true);
		btnModificarPersona.setEnabled(false);
		btnEliminarPersona.setEnabled(false);
		btnLimpiarPersona.setEnabled(true);
		setFotoInicial();
	}
	
	private void setPanelPersonaModificar(Persona pers) {
		comboRol.setSelectedItem(pers.getRol());
		tf_ID.setText(Integer.toString(pers.getId()));
		tf_ID.setEditable(false);
		tf_nombre.setText(FuncionesStrings.capitalize(pers.getNombre()));
		tf_nombre.setEditable(true);
		btnAgregarPersona.setEnabled(false);
		btnModificarPersona.setEnabled(true);
		btnEliminarPersona.setEnabled(true);
		btnLimpiarPersona.setEnabled(true);
		try {
			setFotoPorCli(pers.getId());
		} catch (Exception e) {
			setFotoInicial();
		}
	}
	
	private void setPanelEquipoModificar(Equipo equipo) {
		tf_nombre_equipo.setText(equipo.getNombreDelProyecto().toUpperCase());
		
		Object[] integrantes=new Object[equipo.getIntegrantes().size()];
		Integer[] idsIntegrantes = new Integer[equipo.getIntegrantes().size()];
		int cont = 0;
		for (Integer id: equipo.getIntegrantes()) {
			integrantes[cont] = FuncionesStrings.capitalize(contenedor.getSistema().getPersonaPorId(id).getNombre())+" - ["+contenedor.getSistema().getPersonaPorId(id).getRol().toString().toUpperCase()+"]";
			idsIntegrantes[cont] = id;
			cont++;
		}
		
		listaIntegrantes.setListData(integrantes);
		  
		RenderListaPersonas rl=new RenderListaPersonas(integrantes, idsIntegrantes);
		  
		listaIntegrantes.setCellRenderer(rl);
		  
		  
	}
	
	private void setPanelEquipoEnBlanco() {
		tf_nombre_equipo.setText("");
		ta_datosEquipo.setText("");
		listaIntegrantes.setListData(new Object[0]);
	}
	
	private void setPanelCondicionAgregar() {
		comboLideresMin.setSelectedItem(1);
		comboLideresMax.setSelectedItem(1);
		comboArquitectosMin.setSelectedItem(1);
		comboArquitectosMax.setSelectedItem(1);
		comboProgramadoresMin.setSelectedItem(1);
		comboProgramadoresMax.setSelectedItem(1);
		comboTestersMin.setSelectedItem(1);
		comboTestersMax.setSelectedItem(1);
		td_nombre_proyecto.setText("");
		td_nombre_proyecto.setEditable(true);
		ta_idea.setText("");
		tf_nombre.setEditable(true);
		btnAgregarCond.setEnabled(true);
		btnModificarCond.setEnabled(false);
		btnEliminarCond.setEnabled(false);
		btnLimpiarCond.setEnabled(true);
		btn_fuerzaBruta.setEnabled(false);
		btn_heuristica.setEnabled(false);
	}
	
	private void setPanelCondicionModificar(CondicionesEquipo condiciones) {
		comboLideresMin.setSelectedItem(condiciones.getMinLideres());
		comboLideresMax.setSelectedItem(condiciones.getMaxLideres());
		comboArquitectosMin.setSelectedItem(condiciones.getMinArquitectos());
		comboArquitectosMax.setSelectedItem(condiciones.getMaxArquitectos());
		comboProgramadoresMin.setSelectedItem(condiciones.getMinProgramadores());
		comboProgramadoresMax.setSelectedItem(condiciones.getMaxProgramadores());
		comboTestersMin.setSelectedItem(condiciones.getMinTesters());
		comboTestersMax.setSelectedItem(condiciones.getMaxTesters());
		
		td_nombre_proyecto.setText(condiciones.getNombreDelProyecto().toUpperCase());
		td_nombre_proyecto.setEditable(false);
		ta_idea.setText(condiciones.getIdeaDelProyecto());
		tf_nombre.setEditable(true);
		btnAgregarCond.setEnabled(false);
		btnModificarCond.setEnabled(true);
		btnEliminarCond.setEnabled(true);
		btnLimpiarCond.setEnabled(true);
		btn_fuerzaBruta.setEnabled(true);
		btn_heuristica.setEnabled(true);
	}
	
	// *******************
	// * FUNCIONES FOTOS *
	// *******************
	
	private void setFotoInicial() {
        fotoPersona = FuncionesImagenes.getImageIconResized("src/vista/img/nofoto.png", lbFotoPersona.getWidth(), lbFotoPersona.getHeight());
        lbFotoPersona.setIcon(fotoPersona);
	}
	
	private void setFotoPorCli(int cod) {
		fotoPersona = FuncionesImagenes.getImageIconResized("fotos/"+Integer.toString(cod)+".jpg", lbFotoPersona.getWidth(), lbFotoPersona.getHeight());
		lbFotoPersona.setIcon(fotoPersona);
    }
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 1200, 650);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panelMenu = new JPanel();
		panelMenu.setBackground(new Color(0, 0, 128));
		panelMenu.setBounds(0, 0, 238, 615);
		frame.getContentPane().add(panelMenu);
		panelMenu.setLayout(null);
		
		btnPersonas = new JButton("Personas");
		btnPersonas.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_equipo.png")));
		btnPersonas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelSolapas.setSelectedIndex(0);
			}
		});
		btnPersonas.setBackground(Color.WHITE);
		btnPersonas.setBounds(12, 173, 214, 52);
		panelMenu.add(btnPersonas);
		
		btnEquipos = new JButton("Equipos");
		btnEquipos.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_personas.png")));
		btnEquipos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelSolapas.setSelectedIndex(1);
			}
		});
		btnEquipos.setBackground(Color.WHITE);
		btnEquipos.setBounds(12, 238, 214, 52);
		panelMenu.add(btnEquipos);
		
		btnCondiciones = new JButton("Condiciones");
		btnCondiciones.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_seguro.png")));
		btnCondiciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelSolapas.setSelectedIndex(2);
			}
		});
		btnCondiciones.setBackground(Color.WHITE);
		btnCondiciones.setBounds(12, 303, 214, 52);
		panelMenu.add(btnCondiciones);
		
		btnRelaciones = new JButton("Relaciones");
		btnRelaciones.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_relaciones.png")));
		btnRelaciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelSolapas.setSelectedIndex(3);
			}
		});
		btnRelaciones.setBackground(Color.WHITE);
		btnRelaciones.setBounds(12, 408, 214, 52);
		panelMenu.add(btnRelaciones);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setBackground(Color.WHITE);
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		btnSalir.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_salir.png")));
		btnSalir.setBounds(12, 550, 214, 52);
		panelMenu.add(btnSalir);
		
		panelSolapas = new JTabbedPane(JTabbedPane.TOP);
		panelSolapas.setBounds(237, 0, 957, 615);
		frame.getContentPane().add(panelSolapas);
		
		JPanel panelPersonas = new JPanel();
		panelPersonas.setBackground(Color.WHITE);
		panelSolapas.addTab("Personas", null, panelPersonas, null);
		panelPersonas.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setToolTipText("Clic en la persona para ver y modificar sus datos.");
		scrollPane.setBounds(31, 151, 444, 396);
		panelPersonas.add(scrollPane);
		
		tablaPersonas = new JTable();
		tablaPersonas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int codSel = (int) tablaPersonas.getValueAt(tablaPersonas.getSelectedRow(), 0);
				setPanelPersonaModificar(contenedor.getSistema().getPersonaPorId(codSel));
			}
		});
		scrollPane.setViewportView(tablaPersonas);
		
		JPanel panelTopPersonas = new JPanel();
		panelTopPersonas.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelTopPersonas.setBackground(Color.WHITE);
		panelTopPersonas.setBounds(0, 0, 952, 69);
		panelPersonas.add(panelTopPersonas);
		panelTopPersonas.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(0, 0, 128));
		panel_1.setBounds(0, 0, 23, 69);
		panelTopPersonas.add(panel_1);
		
		JLabel lbPersonas = new JLabel("Personas");
		lbPersonas.setFont(new Font("Tahoma", Font.BOLD, 26));
		lbPersonas.setBounds(98, 0, 154, 69);
		panelTopPersonas.add(lbPersonas);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_equipo.png")));
		lblNewLabel_1.setBounds(35, 0, 64, 69);
		panelTopPersonas.add(lblNewLabel_1);
		
		JPanel panelPersona = new JPanel();
		panelPersona.setBorder(new TitledBorder(null, "Datos de la Persona", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelPersona.setBounds(497, 151, 427, 396);
		panelPersonas.add(panelPersona);
		panelPersona.setLayout(null);
		
		JPanel panelFotoPersona = new JPanel();
		panelFotoPersona.setBackground(Color.WHITE);
		panelFotoPersona.setBorder(new TitledBorder(null, "Foto", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelFotoPersona.setBounds(248, 33, 167, 182);
		panelPersona.add(panelFotoPersona);
		panelFotoPersona.setLayout(null);
		
		lbFotoPersona = new JLabel("");
		lbFotoPersona.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				 if (!tf_ID.getText().equals("")) {
					 if (windowVerFoto != null) {//si existe una venta, la cierra.
						 windowVerFoto.dispose();
				     }
					 windowVerFoto = new VerFotoPersona(contenedor.getSistema().getPersonaPorId(Integer.parseInt(tf_ID.getText())));
					 windowVerFoto.setTitle("Ver Foto");
					 windowVerFoto.setVisible(true);	 
				 }
				 
			}
		});
		lbFotoPersona.setBounds(12, 23, 143, 146);
		panelFotoPersona.add(lbFotoPersona);
		
		tf_ID = new JTextField();
		tf_ID.setEditable(false);
		tf_ID.setBounds(12, 63, 66, 22);
		panelPersona.add(tf_ID);
		tf_ID.setColumns(10);
		
		JLabel lblId = new JLabel("ID:");
		lblId.setBounds(12, 43, 56, 16);
		panelPersona.add(lblId);
		
		JLabel lb_Nombre = new JLabel("Nombre:");
		lb_Nombre.setBounds(12, 104, 56, 16);
		panelPersona.add(lb_Nombre);
		
		tf_nombre = new JTextField();
		tf_nombre.setBounds(12, 125, 210, 22);
		panelPersona.add(tf_nombre);
		tf_nombre.setColumns(10);
		
		comboRol = new JComboBox();
		comboRol.setModel(new DefaultComboBoxModel(Roles.values()));
		comboRol.setBounds(12, 193, 210, 22);
		panelPersona.add(comboRol);
		
		JLabel lblRol = new JLabel("Rol:");
		lblRol.setBounds(12, 171, 56, 16);
		panelPersona.add(lblRol);
		
		JPanel panelAccionesPersona = new JPanel();
		panelAccionesPersona.setBorder(new TitledBorder(null, "Acciones", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelAccionesPersona.setBounds(12, 244, 403, 139);
		panelPersona.add(panelAccionesPersona);
		panelAccionesPersona.setLayout(null);
		
		btnAgregarPersona = new JButton("Agregar");
		btnAgregarPersona.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_add.png")));
		btnAgregarPersona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Persona personaNew = new Persona();
				personaNew.setNombre(tf_nombre.getText());
				personaNew.setRol(getRolSeleccionadoEnPersona());
				try {
					contenedor.getSistema().agregarPersona(personaNew);	
					contenedor.getSistema().guardarDatosJSON();
				} catch (Exception e) {
					Dialogs.infoDialog(e.getMessage(), "INFO");
				}
				
				
				cargaTablaPersonas();
				cargaTablaPersonasRelaciones();
				setPanelPersonaAgregar();
			}
		});
		btnAgregarPersona.setBounds(12, 26, 134, 44);
		panelAccionesPersona.add(btnAgregarPersona);
		
		btnModificarPersona = new JButton("Modificar");
		btnModificarPersona.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_check.png")));
		btnModificarPersona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int dialogResult = JOptionPane.showConfirmDialog(null, "Desea modificar la persona con ID: "+tf_ID.getText()+"?\n\nNuevo nombre: "+FuncionesStrings.capitalize(tf_nombre.getText())+"\nNuevo rol: "+comboRol.getSelectedItem(), "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		        if(dialogResult == JOptionPane.YES_OPTION){
		        	Persona persona = new Persona();
		        	persona.setId(Integer.parseInt(tf_ID.getText()));
		        	persona.setNombre(tf_nombre.getText());
		        	persona.setRol((Roles)comboRol.getSelectedItem());
		        	contenedor.getSistema().actualizarPersona(persona);
		        	contenedor.getSistema().guardarDatosJSON();
		        	cargaTablaPersonas();
		        	cargaTablaPersonasRelaciones();
		            setPanelPersonaAgregar();
		        }
			}
		});
		btnModificarPersona.setBounds(12, 83, 134, 44);
		panelAccionesPersona.add(btnModificarPersona);
		
		btnEliminarPersona = new JButton("Eliminar");
		btnEliminarPersona.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_del.png")));
		btnEliminarPersona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
		        int dialogResult = JOptionPane.showConfirmDialog(null, "Desea eliminar a "+FuncionesStrings.capitalize(tf_nombre.getText())+"?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		        if(dialogResult == JOptionPane.YES_OPTION){
		            contenedor.getSistema().eliminarPersonaPorId(Integer.parseInt(tf_ID.getText()));
		            contenedor.getSistema().guardarDatosJSON();
		            cargaTablaPersonas();
		            cargaTablaPersonasRelaciones();
		            setPanelPersonaAgregar();
		        }
			}
		});
		btnEliminarPersona.setBounds(158, 26, 134, 44);
		panelAccionesPersona.add(btnEliminarPersona);
		
		btnLimpiarPersona = new JButton("Limpiar");
		btnLimpiarPersona.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/ico_limpiar.png")));
		btnLimpiarPersona.setToolTipText("Limpiar");
		btnLimpiarPersona.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setPanelPersonaAgregar();
			}
		});
		btnLimpiarPersona.setBounds(158, 83, 134, 44);
		panelAccionesPersona.add(btnLimpiarPersona);
		
		JPanel panel_6 = new JPanel();
		panel_6.setLayout(null);
		panel_6.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_6.setBackground(Color.WHITE);
		panel_6.setBounds(31, 82, 444, 56);
		panelPersonas.add(panel_6);
		
		JPanel panel_7 = new JPanel();
		panel_7.setBackground(new Color(0, 0, 128));
		panel_7.setBounds(0, 0, 10, 56);
		panel_6.add(panel_7);
		
		tf_nomBusqueda = new JTextField();
		tf_nomBusqueda.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				cargaTablaPersonas();
			}
		});
		tf_nomBusqueda.setBounds(184, 15, 195, 22);
		panel_6.add(tf_nomBusqueda);
		tf_nomBusqueda.setColumns(10);
		
		JLabel lblNombre = new JLabel("Buscar por Nombre:");
		lblNombre.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btnlupa.png")));
		lblNombre.setBounds(25, 13, 161, 27);
		panel_6.add(lblNombre);
		
		JPanel panelEquipos = new JPanel();
		panelEquipos.setBackground(Color.WHITE);
		panelEquipos.setLayout(null);
		panelSolapas.addTab("Equipos", null, panelEquipos, null);
		
		JPanel panelTopEquipos = new JPanel();
		panelTopEquipos.setLayout(null);
		panelTopEquipos.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelTopEquipos.setBackground(Color.WHITE);
		panelTopEquipos.setBounds(0, 0, 952, 69);
		panelEquipos.add(panelTopEquipos);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(new Color(0, 0, 128));
		panel_3.setBounds(0, 0, 23, 69);
		panelTopEquipos.add(panel_3);
		
		JLabel label_3 = new JLabel("");
		label_3.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_personas.png")));
		label_3.setBounds(39, 0, 64, 69);
		panelTopEquipos.add(label_3);
		
		JLabel lblEquipos = new JLabel("Equipos");
		lblEquipos.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblEquipos.setBounds(102, 0, 154, 69);
		panelTopEquipos.add(lblEquipos);
		
		JScrollPane scrollPaneTablaEquipos = new JScrollPane();
		scrollPaneTablaEquipos.setToolTipText("Clic en la persona para ver y modificar sus datos.");
		scrollPaneTablaEquipos.setBounds(29, 151, 444, 396);
		panelEquipos.add(scrollPaneTablaEquipos);
		
		tablaEquipos = new JTable();
		tablaEquipos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String nomSel = (String) tablaEquipos.getValueAt(tablaEquipos.getSelectedRow(), 0);
				Equipo equipo = contenedor.getSistema().getEquipoPorNombre(nomSel);
				setPanelEquipoModificar(equipo);
				ta_datosEquipo.setText(contenedor.getSistema().getDatosEquipo(equipo));
			}
		});
		scrollPaneTablaEquipos.setViewportView(tablaEquipos);
		
		JPanel panelDatosEquipo = new JPanel();
		panelDatosEquipo.setLayout(null);
		panelDatosEquipo.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Datos del Equipo", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelDatosEquipo.setBounds(497, 82, 427, 465);
		panelEquipos.add(panelDatosEquipo);
		
		JLabel label_2 = new JLabel("Nombre:");
		label_2.setBounds(12, 33, 56, 16);
		panelDatosEquipo.add(label_2);
		
		tf_nombre_equipo = new JTextField();
		tf_nombre_equipo.setText("");
		tf_nombre_equipo.setEditable(true);
		tf_nombre_equipo.setColumns(10);
		tf_nombre_equipo.setBounds(12, 54, 210, 22);
		panelDatosEquipo.add(tf_nombre_equipo);
		
		JLabel lblIntegrantes = new JLabel("Integrantes:");
		lblIntegrantes.setBounds(12, 99, 117, 16);
		panelDatosEquipo.add(lblIntegrantes);
		
		JScrollPane scrollListaIntegrantes = new JScrollPane();
		scrollListaIntegrantes.setBounds(12, 128, 403, 240);
		panelDatosEquipo.add(scrollListaIntegrantes);
		
		listaIntegrantes = new JList();
		scrollListaIntegrantes.setViewportView(listaIntegrantes);
		
		JButton btn_Eliminar_Equipo = new JButton("Eliminar Equipo");
		btn_Eliminar_Equipo.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_del.png")));
		btn_Eliminar_Equipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int dialogResult = JOptionPane.showConfirmDialog(null, "Desea eliminar el equipo: \n\n"+FuncionesStrings.capitalize(tf_nombre_equipo.getText())+"?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		        if(dialogResult == JOptionPane.YES_OPTION){
		            contenedor.getSistema().eliminarEquipoPorNombre(tf_nombre_equipo.getText());
		            contenedor.getSistema().guardarDatosJSON();
		            cargaTablaEquipos();
		            setPanelEquipoEnBlanco();
		        }
			}
		});
		btn_Eliminar_Equipo.setBounds(234, 44, 181, 34);
		panelDatosEquipo.add(btn_Eliminar_Equipo);
		
		JScrollPane scrollDatosEquipo = new JScrollPane();
		scrollDatosEquipo.setBounds(12, 381, 403, 71);
		panelDatosEquipo.add(scrollDatosEquipo);
		
		ta_datosEquipo = new JTextArea();
		ta_datosEquipo.setBackground(SystemColor.control);
		ta_datosEquipo.setEditable(false);
		scrollDatosEquipo.setViewportView(ta_datosEquipo);
		
		JPanel panel_8 = new JPanel();
		panel_8.setLayout(null);
		panel_8.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_8.setBackground(Color.WHITE);
		panel_8.setBounds(29, 82, 444, 56);
		panelEquipos.add(panel_8);
		
		JPanel panel_9 = new JPanel();
		panel_9.setBackground(new Color(0, 0, 128));
		panel_9.setBounds(0, 0, 10, 56);
		panel_8.add(panel_9);
		
		tf_nomEquipoBusqueda = new JTextField();
		tf_nomEquipoBusqueda.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				cargaTablaEquipos();
			}
		});
		tf_nomEquipoBusqueda.setColumns(10);
		tf_nomEquipoBusqueda.setBounds(184, 15, 195, 22);
		panel_8.add(tf_nomEquipoBusqueda);
		
		JLabel label_6 = new JLabel("Buscar por Nombre:");
		label_6.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btnlupa.png")));
		label_6.setBounds(25, 13, 161, 27);
		panel_8.add(label_6);
		
		JPanel panelCondiciones = new JPanel();
		panelCondiciones.setBackground(Color.WHITE);
		panelCondiciones.setLayout(null);
		panelSolapas.addTab("Condiciones", null, panelCondiciones, null);
		
		JPanel panelTopCondiciones = new JPanel();
		panelTopCondiciones.setLayout(null);
		panelTopCondiciones.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelTopCondiciones.setBackground(Color.WHITE);
		panelTopCondiciones.setBounds(0, 0, 952, 69);
		panelCondiciones.add(panelTopCondiciones);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(0, 0, 128));
		panel_2.setBounds(0, 0, 23, 69);
		panelTopCondiciones.add(panel_2);
		
		JLabel label_9 = new JLabel("");
		label_9.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_seguro.png")));
		label_9.setBounds(45, 0, 64, 69);
		panelTopCondiciones.add(label_9);
		
		JLabel lblCondiciones = new JLabel("Condiciones");
		lblCondiciones.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblCondiciones.setBounds(108, 0, 238, 69);
		panelTopCondiciones.add(lblCondiciones);
		
		JScrollPane scrollPaneTablaCondiciones = new JScrollPane();
		scrollPaneTablaCondiciones.setToolTipText("Clic en la condici\u00F3n para ver y modificar sus datos.");
		scrollPaneTablaCondiciones.setBounds(31, 151, 416, 396);
		panelCondiciones.add(scrollPaneTablaCondiciones);
		
		tablaCondiciones = new JTable();
		tablaCondiciones.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String nomSel = (String) tablaCondiciones.getValueAt(tablaCondiciones.getSelectedRow(), 0);
				setPanelCondicionModificar(contenedor.getSistema().getCondicionPorNombre(nomSel));
			}
		});
		scrollPaneTablaCondiciones.setViewportView(tablaCondiciones);
		
		JPanel panelCondicion = new JPanel();
		panelCondicion.setLayout(null);
		panelCondicion.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Datos de la Condici\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelCondicion.setBounds(459, 151, 481, 396);
		panelCondiciones.add(panelCondicion);
		
		JLabel lblNombreDelProyecto = new JLabel("Nombre del Proyecto:");
		lblNombreDelProyecto.setBounds(12, 24, 320, 16);
		panelCondicion.add(lblNombreDelProyecto);
		
		td_nombre_proyecto = new JTextField();
		td_nombre_proyecto.setText("");
		td_nombre_proyecto.setEditable(true);
		td_nombre_proyecto.setColumns(10);
		td_nombre_proyecto.setBounds(12, 45, 210, 22);
		panelCondicion.add(td_nombre_proyecto);
		
		comboLideresMin = new JComboBox();
		comboLideresMin.setBounds(16, 100, 59, 22);
		panelCondicion.add(comboLideresMin);
		
		JLabel lblLderes = new JLabel("L\u00EDderes:");
		lblLderes.setBounds(16, 78, 96, 16);
		panelCondicion.add(lblLderes);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(12, 194, 457, 58);
		panelCondicion.add(scrollPane_1);
		
		ta_idea = new JTextArea();
		scrollPane_1.setViewportView(ta_idea);
		
		JLabel lblNewLabel = new JLabel("Idea del proyecto:");
		lblNewLabel.setBounds(12, 170, 140, 16);
		panelCondicion.add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setBorder(new TitledBorder(null, "Acciones", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(12, 265, 457, 118);
		panelCondicion.add(panel);
		
		btnAgregarCond = new JButton("Agregar");
		btnAgregarCond.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_add.png")));
		btnAgregarCond.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CondicionesEquipo cond = new CondicionesEquipo();
	        	cond.setNombreDelProyecto(td_nombre_proyecto.getText());
	        	cond.setMinLideres((int)comboLideresMin.getSelectedItem());
	        	cond.setMaxLideres((int)comboLideresMax.getSelectedItem());
	        	cond.setMinArquitectos((int)comboArquitectosMin.getSelectedItem());
	        	cond.setMaxArquitectos((int)comboArquitectosMax.getSelectedItem());
	        	cond.setMinProgramadores((int)comboProgramadoresMin.getSelectedItem());
	        	cond.setMaxProgramadores((int)comboProgramadoresMax.getSelectedItem());
	        	cond.setMinTesters((int)comboTestersMin.getSelectedItem());
	        	cond.setMaxTesters((int)comboTestersMax.getSelectedItem());
	        	cond.setIdeaDelProyecto(ta_idea.getText());
	        	try {
	        		contenedor.getSistema().agregarCondicion(cond);	
	        		contenedor.getSistema().guardarDatosJSON();
	        	} catch (Exception ex) {
	        		Dialogs.infoDialog(ex.getMessage(), "INFO");
	        	}
	        	
	            cargaTablaCondiciones();
	            setPanelCondicionAgregar();
			}
		});
		btnAgregarCond.setEnabled(true);
		btnAgregarCond.setBounds(12, 25, 128, 34);
		panel.add(btnAgregarCond);
		
		btnModificarCond = new JButton("Modificar");
		btnModificarCond.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_check.png")));
		btnModificarCond.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int dialogResult = JOptionPane.showConfirmDialog(null, "Desea modificar la condición: "+td_nombre_proyecto.getText()+"?","Confirmar modificación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		        if(dialogResult == JOptionPane.YES_OPTION){
		        	CondicionesEquipo cond = new CondicionesEquipo();
		        	cond.setNombreDelProyecto(td_nombre_proyecto.getText());
		        	cond.setMinLideres((int)comboLideresMin.getSelectedItem());
		        	cond.setMaxLideres((int)comboLideresMax.getSelectedItem());
		        	cond.setMinArquitectos((int)comboArquitectosMin.getSelectedItem());
		        	cond.setMaxArquitectos((int)comboArquitectosMax.getSelectedItem());
		        	cond.setMinProgramadores((int)comboProgramadoresMin.getSelectedItem());
		        	cond.setMaxProgramadores((int)comboProgramadoresMax.getSelectedItem());
		        	cond.setMinTesters((int)comboTestersMin.getSelectedItem());
		        	cond.setMaxTesters((int)comboTestersMax.getSelectedItem());
		        	cond.setIdeaDelProyecto(ta_idea.getText());
		        	
		        	try {
		        		
		        		if(contenedor.getSistema().actualizarCondicion(cond)){
		        			contenedor.getSistema().guardarDatosJSON();
		        		}
		        	} catch (Exception ex) {
		        		Dialogs.infoDialog(ex.getMessage(), "INFO");
		        	}
		            
		        	
		        	cargaTablaCondiciones();
		            setPanelCondicionAgregar();
		        }
			}
		});
		btnModificarCond.setEnabled(false);
		btnModificarCond.setBounds(12, 72, 128, 34);
		panel.add(btnModificarCond);
		
		btnEliminarCond = new JButton("Eliminar");
		btnEliminarCond.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_del.png")));
		btnEliminarCond.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int dialogResult = JOptionPane.showConfirmDialog(null, "Desea eliminar la condición "+FuncionesStrings.capitalize(td_nombre_proyecto.getText())+"?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		        if(dialogResult == JOptionPane.YES_OPTION){
		            contenedor.getSistema().eliminarCondicionPorNombre(td_nombre_proyecto.getText());
		            contenedor.getSistema().guardarDatosJSON();
		            cargaTablaCondiciones();
		            setPanelCondicionAgregar();
		        }
			}
		});
		btnEliminarCond.setEnabled(false);
		btnEliminarCond.setBounds(152, 25, 128, 34);
		panel.add(btnEliminarCond);
		
		btnLimpiarCond = new JButton("Limpiar");
		btnLimpiarCond.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/ico_limpiar.png")));
		btnLimpiarCond.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setPanelCondicionAgregar();
			}
		});
		btnLimpiarCond.setEnabled(true);
		btnLimpiarCond.setBounds(152, 72, 128, 34);
		panel.add(btnLimpiarCond);
		
		btn_fuerzaBruta = new JButton("<html><center>Generar Equipo<br>Fuerza Bruta</center></html>");
		btn_fuerzaBruta.setEnabled(false);
		btn_fuerzaBruta.setVerifyInputWhenFocusTarget(false);
		btn_fuerzaBruta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					WorkerCreaEquipo workerEquipo = new WorkerCreaEquipo();
		            workerEquipo.setCondicion(contenedor.getSistema().getCondicionPorNombre(td_nombre_proyecto.getText()));
		            workerEquipo.setTipoSolucion(TipoSolucion.FuerzaBruta);
		            workerEquipo.execute();	
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				
			}
		});
		btn_fuerzaBruta.setBounds(292, 25, 153, 34);
		panel.add(btn_fuerzaBruta);
		
		btn_heuristica = new JButton("<html><center>Generar Equipo<br>Heur\u00EDstica</center></html>");
		btn_heuristica.setEnabled(false);
		btn_heuristica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					WorkerCreaEquipo workerEquipo = new WorkerCreaEquipo();
		            workerEquipo.setCondicion(contenedor.getSistema().getCondicionPorNombre(td_nombre_proyecto.getText()));
		            workerEquipo.setTipoSolucion(TipoSolucion.Heurística);
		            workerEquipo.execute();	
				} catch (Exception ex) {
					System.out.println(ex.getMessage());
				}
			}
		});
		btn_heuristica.setBounds(292, 72, 153, 34);
		panel.add(btn_heuristica);
		
		comboLideresMax = new JComboBox<Integer>();
		comboLideresMax.setBounds(16, 135, 59, 22);
		panelCondicion.add(comboLideresMax);
		
		JLabel lblMin = new JLabel("m\u00EDn.");
		lblMin.setBounds(87, 103, 25, 16);
		panelCondicion.add(lblMin);
		
		JLabel lblMx = new JLabel("m\u00E1x.");
		lblMx.setBounds(87, 138, 28, 16);
		panelCondicion.add(lblMx);
		
		JLabel label = new JLabel("m\u00E1x.");
		label.setBounds(194, 138, 28, 16);
		panelCondicion.add(label);
		
		comboArquitectosMax = new JComboBox<Integer>();
		comboArquitectosMax.setBounds(123, 135, 59, 22);
		panelCondicion.add(comboArquitectosMax);
		
		comboArquitectosMin = new JComboBox<Integer>();
		comboArquitectosMin.setBounds(123, 100, 59, 22);
		panelCondicion.add(comboArquitectosMin);
		
		JLabel label_1 = new JLabel("m\u00EDn.");
		label_1.setBounds(194, 103, 25, 16);
		panelCondicion.add(label_1);
		
		JLabel lblArquitectos = new JLabel("Arquitectos:");
		lblArquitectos.setBounds(123, 78, 96, 16);
		panelCondicion.add(lblArquitectos);
		
		JLabel label_4 = new JLabel("m\u00E1x.");
		label_4.setBounds(304, 138, 28, 16);
		panelCondicion.add(label_4);
		
		comboProgramadoresMax = new JComboBox<Integer>();
		comboProgramadoresMax.setBounds(233, 135, 59, 22);
		panelCondicion.add(comboProgramadoresMax);
		
		comboProgramadoresMin = new JComboBox<Integer>();
		comboProgramadoresMin.setBounds(233, 100, 59, 22);
		panelCondicion.add(comboProgramadoresMin);
		
		JLabel label_5 = new JLabel("m\u00EDn.");
		label_5.setBounds(304, 103, 25, 16);
		panelCondicion.add(label_5);
		
		JLabel lblProgramadores = new JLabel("Programadores:");
		lblProgramadores.setBounds(233, 78, 96, 16);
		panelCondicion.add(lblProgramadores);
		
		JLabel label_7 = new JLabel("m\u00E1x.");
		label_7.setBounds(415, 138, 28, 16);
		panelCondicion.add(label_7);
		
		comboTestersMax = new JComboBox<Integer>();
		comboTestersMax.setBounds(344, 135, 59, 22);
		panelCondicion.add(comboTestersMax);
		
		comboTestersMin = new JComboBox<Integer>();
		comboTestersMin.setBounds(344, 100, 59, 22);
		panelCondicion.add(comboTestersMin);
		
		JLabel label_8 = new JLabel("m\u00EDn.");
		label_8.setBounds(415, 103, 25, 16);
		panelCondicion.add(label_8);
		
		JLabel lblTesters = new JLabel("Testers:");
		lblTesters.setBounds(344, 78, 96, 16);
		panelCondicion.add(lblTesters);
		
		JPanel panel_10 = new JPanel();
		panel_10.setLayout(null);
		panel_10.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_10.setBackground(Color.WHITE);
		panel_10.setBounds(31, 82, 416, 56);
		panelCondiciones.add(panel_10);
		
		JPanel panel_11 = new JPanel();
		panel_11.setBackground(new Color(0, 0, 128));
		panel_11.setBounds(0, 0, 10, 56);
		panel_10.add(panel_11);
		
		tf_condBusqueda = new JTextField();
		tf_condBusqueda.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				cargaTablaCondiciones();
			}
		});
		tf_condBusqueda.setColumns(10);
		tf_condBusqueda.setBounds(184, 15, 195, 22);
		panel_10.add(tf_condBusqueda);
		
		JLabel label_10 = new JLabel("Buscar por Nombre:");
		label_10.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btnlupa.png")));
		label_10.setBounds(25, 13, 161, 27);
		panel_10.add(label_10);
		
		JPanel panelRelaciones = new JPanel();
		panelRelaciones.setBackground(Color.WHITE);
		panelRelaciones.setLayout(null);
		panelSolapas.addTab("Relaciones", null, panelRelaciones, null);
		
		JPanel panelTopRelaciones = new JPanel();
		panelTopRelaciones.setLayout(null);
		panelTopRelaciones.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelTopRelaciones.setBackground(Color.WHITE);
		panelTopRelaciones.setBounds(0, 0, 952, 69);
		panelRelaciones.add(panelTopRelaciones);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(new Color(0, 0, 128));
		panel_5.setBounds(0, 0, 23, 69);
		panelTopRelaciones.add(panel_5);
		
		JLabel label_11 = new JLabel("");
		label_11.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_relaciones.png")));
		label_11.setBounds(49, 0, 64, 69);
		panelTopRelaciones.add(label_11);
		
		JLabel lblRelaciones = new JLabel("Relaciones");
		lblRelaciones.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblRelaciones.setBounds(112, 0, 154, 69);
		panelTopRelaciones.add(lblRelaciones);
		
		JScrollPane scrollPersonasIncompat = new JScrollPane();
		scrollPersonasIncompat.setToolTipText("");
		scrollPersonasIncompat.setBounds(31, 151, 444, 396);
		panelRelaciones.add(scrollPersonasIncompat);
		
		tablaPersonasRelaciones = new JTable();
		tablaPersonasRelaciones.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int[] filasSeleccionadas = tablaPersonasRelaciones.getSelectedRows(); 
				if (filasSeleccionadas.length>2) {
					Dialogs.infoDialog("Solo debe seleccionar dos personas para\nmarcarlas como incompatibles.", "INFO");
					tablaPersonasRelaciones.clearSelection();	
				} 
				
			}
		});
		scrollPersonasIncompat.setViewportView(tablaPersonasRelaciones);
		
		JPanel panelIncompatibilidades = new JPanel();
		panelIncompatibilidades.setLayout(null);
		panelIncompatibilidades.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Lista de Incompatibilidades", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelIncompatibilidades.setBounds(497, 151, 427, 396);
		panelRelaciones.add(panelIncompatibilidades);
		
		JPanel panel_4 = new JPanel();
		panel_4.setLayout(null);
		panel_4.setBorder(new TitledBorder(null, "Acciones", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setBounds(12, 272, 403, 111);
		panelIncompatibilidades.add(panel_4);
		
		JButton btnAgregarIncompatibilidad = new JButton("Agregar");
		btnAgregarIncompatibilidad.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_add.png")));
		btnAgregarIncompatibilidad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int[] filasSeleccionadas = tablaPersonasRelaciones.getSelectedRows(); 
				if (filasSeleccionadas.length!=2) {
					Dialogs.infoDialog("Debe seleccionar dos personas para\nmarcarlas como incompatibles.", "INFO");
					tablaPersonasRelaciones.clearSelection();	
				}  else {
					int id_1 = (int) tablaPersonasRelaciones.getValueAt(filasSeleccionadas[0], 0);
					int id_2 = (int) tablaPersonasRelaciones.getValueAt(filasSeleccionadas[1], 0);
					ParesIncompatibles persInc = new ParesIncompatibles(id_1,id_2);	
					try {
						contenedor.getSistema().agregarParIncompatible(persInc);
						contenedor.getSistema().guardarDatosJSON();
					} catch(Exception e) {
						Dialogs.infoDialog(e.getMessage(), "INFO");
					}
					
					cargarListaIncompatibles();
				}
				
			}
		});
		btnAgregarIncompatibilidad.setEnabled(true);
		btnAgregarIncompatibilidad.setBounds(12, 24, 135, 74);
		panel_4.add(btnAgregarIncompatibilidad);
		
		JButton btn_Eliminar_Incompat = new JButton("Eliminar");
		btn_Eliminar_Incompat.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btn_del.png")));
		btn_Eliminar_Incompat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (listaIncompatibilidades.getSelectedValue() != null) {
					int dialogResult = JOptionPane.showConfirmDialog(null, "Desea eliminar la incompatibilidad: \n\n"+FuncionesStrings.capitalize(listaIncompatibilidades.getSelectedValue().toString())+"?", "Confirmar eliminación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
			        if(dialogResult == JOptionPane.YES_OPTION){
			        	contenedor.getSistema().eliminarIncompatibilidadPorStringNombre(listaIncompatibilidades.getSelectedValue().toString());
			        	cargarListaIncompatibles();
			        	contenedor.getSistema().guardarDatosJSON();;
			        }
				}
				
			}
		});
		btn_Eliminar_Incompat.setBounds(256, 24, 135, 74);
		panel_4.add(btn_Eliminar_Incompat);
		
		JScrollPane scrollListaIncompatiblidades = new JScrollPane();
		scrollListaIncompatiblidades.setBounds(12, 54, 403, 205);
		panelIncompatibilidades.add(scrollListaIncompatiblidades);
		
		listaIncompatibilidades = new JList();
		scrollListaIncompatiblidades.setViewportView(listaIncompatibilidades);
		
		JLabel lblPersonasIncompatibles = new JLabel("Personas Incompatibles:");
		lblPersonasIncompatibles.setBounds(12, 29, 228, 16);
		panelIncompatibilidades.add(lblPersonasIncompatibles);
		
		JPanel panel_12 = new JPanel();
		panel_12.setLayout(null);
		panel_12.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel_12.setBackground(Color.WHITE);
		panel_12.setBounds(31, 82, 444, 56);
		panelRelaciones.add(panel_12);
		
		JPanel panel_13 = new JPanel();
		panel_13.setBackground(new Color(0, 0, 128));
		panel_13.setBounds(0, 0, 10, 56);
		panel_12.add(panel_13);
		
		tf_nombreRelaciones = new JTextField();
		tf_nombreRelaciones.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				cargaTablaPersonasRelaciones();
			}
		});
		tf_nombreRelaciones.setColumns(10);
		tf_nombreRelaciones.setBounds(184, 15, 195, 22);
		panel_12.add(tf_nombreRelaciones);
		
		JLabel label_12 = new JLabel("Buscar por Nombre:");
		label_12.setIcon(new ImageIcon(Principal.class.getResource("/vista/img/btnlupa.png")));
		label_12.setBounds(25, 13, 161, 27);
		panel_12.add(label_12);
	}
}
