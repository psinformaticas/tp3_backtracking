package vista;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Hashtable;






import javax.imageio.ImageIO;
import javax.swing.*;


public class RenderListaIncompatibles extends JLabel implements ListCellRenderer{
 
 Hashtable<Object, ImageIcon> elementos;
 ImageIcon icononulo= FuncionesImagenes.getImageIconResized("src/vista/img/nofoto.png", 64, 64);
 
 public RenderListaIncompatibles(Object[] array, String[] idsIncompatibles){
	 elementos=new Hashtable<Object, ImageIcon>();
	 ImageIcon[] fotos = new ImageIcon[array.length];
	 
	 for (int i=0; i<array.length; i++) {
		 String[] arrAux = idsIncompatibles[i].split("-");
		 try {
			 BufferedImage img1 = ImageIO.read(new File("fotos/"+arrAux[0]+".jpg"));
		     BufferedImage img2= ImageIO.read(new File("fotos/"+arrAux[1]+".jpg"));
		     BufferedImage joinedImg = FuncionesImagenes.unirBufferedImages(img1,img2);
		     
		     
		     fotos[i] = FuncionesImagenes.getImageIconResizedDesdeBuffImage(joinedImg, 64, 64); 
		    		 	 
		 } catch (Exception e) {
			 //SI NO TIENE FOTO CARGADA
			 fotos[i] = FuncionesImagenes.getImageIconResized("src/vista/img/nofoto.png", 64, 64);
		 }
		 
		 elementos.put(array[i], fotos[i]);
	 }
 
 }


 @Override
 public Component getListCellRendererComponent(JList list, Object value,int index, boolean isSelected, boolean cellHasFocus) {
  if(elementos.get(value)!=null){
   setIcon(elementos.get(value));
   setText(""+value);
   if(isSelected){
       setFont(new Font("Verdana",Font.BOLD,16));
   }else{
    setFont(null);
   }
  }else{
     setIcon(icononulo);
     setText(""+value);
     if(isSelected){
       setFont(new Font("Verdana",Font.BOLD,16));
   }else{
    setFont(null);
   }
  }
  return this;
 }
}

