package vista;

import java.util.List;

import javax.swing.SwingWorker;

import modelo.CondicionesEquipo;
import modelo.TipoSolucion;

public class WorkerCreaEquipo extends SwingWorker<Double, Integer> {
    
	CondicionesEquipo condicion;
	TipoSolucion tipoSol; 
	
    @Override
    protected Double doInBackground() throws Exception {
    	
        
        
         
        
         
        
    	try {
    		long inicio = System.currentTimeMillis();
	    	if (tipoSol==TipoSolucion.FuerzaBruta) {
	    		Principal.contenedor.generarEquipo(condicion);	
	    	}
	    	if (tipoSol==TipoSolucion.Heurística) {
	    		Principal.contenedor.generarEquipoSegundaVersion(condicion);	
	    	}
	    	long fin = System.currentTimeMillis();
	        double tiempo = (double) ((fin - inicio));
	        
	    	Principal.cargaTablaEquipos();
	    	Principal.contenedor.getSistema().guardarDatosJSON();
	    	Dialogs.infoDialog("El algoritmo de tipo: "+tipoSol.toString()+"\n\nHa demorado: "+tiempo+"ms", "INFO");
    	} catch (Exception e) {
    		Dialogs.infoDialog(e.getMessage(), "INFO");
    	}
    	
       return 100.0;
    }
    

    @Override
    protected void process(List<Integer> chunks) {
        
    }
    
    public void setCondicion(CondicionesEquipo cond) {
    	condicion = cond;
    }
    
    public void setTipoSolucion(TipoSolucion tipoSol) {
    	this.tipoSol = tipoSol;
    }

}
