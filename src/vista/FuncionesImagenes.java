package vista;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class FuncionesImagenes {

	 public static ImageIcon getImageIconResized(String ruta, int ancho, int alto) {
	        
         BufferedImage imgBuff = null;
        try {
            imgBuff = ImageIO.read(new File(ruta));
            
        } catch (IOException e) {
           throw new RuntimeException("Error al leer el archivo");
        }
        
        Image dimg = imgBuff.getScaledInstance(ancho, alto, BufferedImage.SCALE_SMOOTH);
        
        ImageIcon imageIcon = new ImageIcon(dimg);
        
        return imageIcon;
    }
	 
	 public static ImageIcon getImageIconResizedDesdeBuffImage(BufferedImage imgBuff, int ancho, int alto) {
	        
        Image dimg = imgBuff.getScaledInstance(ancho, alto, BufferedImage.SCALE_SMOOTH);
        
        ImageIcon imageIcon = new ImageIcon(dimg);
        
        return imageIcon;
    }
    
     public static BufferedImage getScaledImage(String ruta, int w, int h) throws IOException {
        File file = new File(ruta);
        BufferedImage src=ImageIO.read(file);
        int original_width = src.getWidth();
        int original_height = src.getHeight();
        int bound_width = w;
        int bound_height = h;
        int new_width = original_width;
        int new_height = original_height;

        // first check if we need to scale width
        if (original_width > bound_width) {
            //scale width to fit
            new_width = bound_width;
            //scale height to maintain aspect ratio
            new_height = (new_width * original_height) / original_width;
        }

        // then check if we need to scale even with the new height
        if (new_height > bound_height) {
            //scale height to fit instead
            new_height = bound_height;
            //scale width to maintain aspect ratio
            new_width = (new_height * original_width) / original_height;
        }

        BufferedImage resizedImg = new BufferedImage(new_width, new_height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = resizedImg.createGraphics();
        g2.setBackground(Color.WHITE);
        g2.clearRect(0,0,new_width, new_height);
        g2.drawImage(src, 0, 0, new_width, new_height, null);
        g2.dispose();
        return resizedImg;
    }
           
     public static BufferedImage unirBufferedImages(BufferedImage img1,BufferedImage img2) {

         //do some calculate first
         int offset  = 5;
         int wid = img1.getWidth()+img2.getWidth()+offset;
         int height = Math.max(img1.getHeight(),img2.getHeight())+offset;
         //create a new buffer and draw two image into the new image
         BufferedImage newImage = new BufferedImage(wid,height, BufferedImage.TYPE_INT_ARGB);
         Graphics2D g2 = newImage.createGraphics();
         Color oldColor = g2.getColor();
         //fill background
         g2.setPaint(Color.WHITE);
         g2.fillRect(0, 0, wid, height);
         //draw image
         g2.setColor(oldColor);
         g2.drawImage(img1, null, 0, 0);
         g2.drawImage(img2, null, img1.getWidth()+offset, 0);
         g2.dispose();
         return newImage;
     }
     

}
