package vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.Color;

import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.JSeparator;
import javax.swing.JButton;

import controlador.FuncionesStrings;
import modelo.Persona;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

public class VerFotoPersona extends JFrame {

	private JPanel contentPane;
	private Persona persona;
	private JLabel lbNombre;
	private JLabel lbFoto;
	private ImageIcon fotoPersona;
	private JButton btnSubir;
	private JButton btnEliminarFoto;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VerFotoPersona frame = new VerFotoPersona();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VerFotoPersona() {
		initialize();
	}
	
	public VerFotoPersona(Persona pers) {
		initialize();
		setLocationRelativeTo(null);
		persona = pers;
		
		lbNombre.setText(FuncionesStrings.capitalize(persona.getNombre()));
		setFoto();
	}

	private void setFoto() {
		try {
			setFotoPorCli(persona.getId());
		} catch (Exception e) {
			setFotoInicial();
		}
	}
	
	private void setFotoInicial() {
        fotoPersona = FuncionesImagenes.getImageIconResized("src/vista/img/nofoto.png", lbFoto.getWidth(), lbFoto.getHeight());
        lbFoto.setIcon(fotoPersona);
	}
	
	private void setFotoPorCli(int cod) {
		fotoPersona = FuncionesImagenes.getImageIconResized("fotos/"+Integer.toString(cod)+".jpg", lbFoto.getWidth(), lbFoto.getHeight());
		lbFoto.setIcon(fotoPersona);
    }
	
	public void initialize() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		setBounds(100, 100, 450, 536);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		

		setResizable(false);
		getContentPane().setBackground(Color.WHITE);
		getContentPane().setLayout(null);
		contentPane.setLayout(null);
		
		lbNombre = new JLabel("lbNombre");
		lbNombre.setFont(new Font("Tahoma", Font.BOLD, 23));
		lbNombre.setBounds(10, 10, 422, 50);
		getContentPane().add(lbNombre);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 57, 422, 13);
		getContentPane().add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 453, 422, 13);
		getContentPane().add(separator_1);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnSalir.setBounds(335, 465, 97, 25);
		getContentPane().add(btnSalir);
		
		lbFoto = new JLabel("");
		lbFoto.setBounds(10, 75, 422, 365);
		contentPane.add(lbFoto);
		
		btnSubir = new JButton("Subir Foto");
		btnSubir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					String rutaFoto = "";
					File directorioProyecto = new File(System.getProperty("user.dir"));
					JFileChooser selectorRuta = new JFileChooser();

					FileNameExtensionFilter filtroTxt = new FileNameExtensionFilter("Archivos de im�genes JPG", "jpg", "text");
					selectorRuta.setFileFilter(filtroTxt);
					selectorRuta.setCurrentDirectory(directorioProyecto);
					int returnValue = 0;
					returnValue = selectorRuta.showSaveDialog(null);
		
					if (returnValue == JFileChooser.APPROVE_OPTION) {
						File selectedFile = selectorRuta.getSelectedFile();
						rutaFoto = selectedFile.getAbsolutePath();
						try {
							ImageIO.write(FuncionesImagenes.getScaledImage(rutaFoto, 1024, 1024), "jpg", new File("fotos/"+Integer.toString(persona.getId())+".jpg"));
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						setFotoPorCli(persona.getId());
					}		
				
			}
		});
		btnSubir.setBounds(10, 465, 97, 25);
		contentPane.add(btnSubir);
		
		btnEliminarFoto = new JButton("Eliminar Foto");
		btnEliminarFoto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File fichero = new File("fotos/"+Integer.toString(persona.getId())+".jpg");
				if (fichero.exists()) {
					fichero.delete();
			    }
				setFotoInicial();
			}
		});
		btnEliminarFoto.setBounds(119, 465, 134, 25);
		contentPane.add(btnEliminarFoto);
	}
}
