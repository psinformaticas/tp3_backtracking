package vista;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.Hashtable;



import javax.swing.*;


public class RenderListaPersonas extends JLabel implements ListCellRenderer{
 
 Hashtable<Object, ImageIcon> elementos;
 ImageIcon icononulo= FuncionesImagenes.getImageIconResized("src/vista/img/nofoto.png", 64, 64);
 
 public RenderListaPersonas(Object[] array, Integer[] idsIntegrantes){
	 elementos=new Hashtable<Object, ImageIcon>();
	 ImageIcon[] fotos = new ImageIcon[array.length];
	 
	 for (int i=0; i<array.length; i++) {
		 String id = Integer.toString(idsIntegrantes[i]);
		 try {
			 
			 fotos[i] = FuncionesImagenes.getImageIconResized("fotos/"+id+".jpg", 64, 64);	 
		 } catch (Exception e) {
			 //SI NO TIENE FOTO CARGADA
			 fotos[i] = FuncionesImagenes.getImageIconResized("src/vista/img/nofoto.png", 64, 64);
		 }
		 
		 elementos.put(array[i], fotos[i]);
	 }
 
 }


 @Override
 public Component getListCellRendererComponent(JList list, Object value,int index, boolean isSelected, boolean cellHasFocus) {
  if(elementos.get(value)!=null){
   setIcon(elementos.get(value));
   setText(""+value);
   if(isSelected){
       setFont(new Font("Verdana",Font.BOLD,16));
   }else{
    setFont(null);
   }
  }else{
     setIcon(icononulo);
     setText(""+value);
     if(isSelected){
       setFont(new Font("Verdana",Font.BOLD,16));
   }else{
    setFont(null);
   }
  }
  return this;
 }
}

