package controlador;

import static org.junit.Assert.*;

import java.util.ArrayList;

import modelo.CondicionesEquipo;
import modelo.Equipo;
import modelo.ParesIncompatibles;
import modelo.Persona;
import modelo.Roles;

import org.junit.Before;
import org.junit.Test;

public class SistemaTest {
	Equipo equipo;
	Sistema sistema;
	ArrayList<Equipo> equipos;
	CondicionesEquipo condicion;
	Persona persona1;
	Persona persona2;
	ParesIncompatibles par;
	
	@Before
	public void inicializar(){
		sistema = new Sistema();
		equipo = new Equipo();
		condicion = new CondicionesEquipo("TEST","test",1,1,1,2,2,4,2,3);
		persona1 = new Persona(1, "Antonio Sala", Roles.Arquitecto);
		persona2 = new Persona(2, "C�sar Cruz", Roles.Lider);
		
		par = new ParesIncompatibles(persona1.getId(), persona2.getId());
		
		equipo.setNombreDelProyecto("TEST");
		
	}
	@Test
	public void agregarEquipoTest() {
		sistema.agregarEquipo(equipo);
		assertTrue(sistema.getEquiposDeTrabajo().size()==1);
	}
	@Test
	public void agregarCondicionTest(){
		sistema.agregarCondicion(condicion);
		assertTrue(sistema.getCondicionesParaEquipos().size()==1);
	}
	@Test
	public void agregarPersonaTest(){
		sistema.agregarPersona(persona1);
		assertTrue(sistema.getPersonas().size()==1);
	}
	@Test
	public void agregarParIncompatibleTest(){
		sistema.agregarParIncompatible(par);
		assertTrue(sistema.getListaIncompatibles().size()==1);
	}
	@Test
	public void eliminarEquipoPorNombreTest(){
		sistema.agregarEquipo(equipo);
		sistema.eliminarEquipoPorNombre("TEST");
		assertTrue(sistema.getEquiposDeTrabajo().isEmpty());
	}
	@Test
	public void eliminarCondicionPorNombreTest(){
		sistema.agregarCondicion(condicion);
		sistema.eliminarCondicionPorNombre("TEST");
		assertTrue(sistema.getCondicionesParaEquipos().isEmpty());
	}
	@Test
	public void eliminarPersonaPorIdTest(){
		sistema.actualizarPersona(persona1);
		sistema.eliminarPersonaPorId(1);
		assertTrue(sistema.getPersonas().isEmpty());
	}
	@Test
	public void eliminarIncompatibilidadPorStringNombreTest(){
		sistema.agregarPersona(persona1);
		sistema.agregarPersona(persona2);
		sistema.agregarParIncompatible(par);
		sistema.eliminarIncompatibilidadPorStringNombre("Antonio Sala - C�sar Cruz");
		assertTrue(sistema.getListaIncompatibles().isEmpty());
	}
	@Test
	public void getMaxIDPersonaTest(){
		sistema.agregarPersona(persona1);
		sistema.agregarPersona(persona2);
		assertEquals(sistema.getMaxIDPersona(),2);
	}
	@Test
	public void getIDPersonaPorNombreTest(){
		sistema.agregarPersona(persona1);
		assertEquals(sistema.getIDPersonaPorNombre("Antonio Sala"),1);
	}
	@Test
	public void nombreDePersonaYaExisteTest(){
		sistema.agregarPersona(persona1);
		assertTrue(sistema.nombreDePersonaYaExiste("Antonio Sala"));
	}
	@Test
	public void existeConflictoEntreEstos2IdsTest(){
		sistema.agregarParIncompatible(par);
		assertTrue(sistema.existeConflictoEntreEstos2Ids(1,2));
	}

}

