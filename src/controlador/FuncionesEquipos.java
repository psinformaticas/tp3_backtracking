package controlador;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import modelo.CondicionesEquipo;
import modelo.Equipo;
import modelo.Persona;
import modelo.Roles;

public class FuncionesEquipos {


	//FUERZA BRUTA
	public static Equipo generarEquipo(Sistema sistema, CondicionesEquipo condicion){

		ArrayList<Persona> integrantes = new ArrayList<>();
		Equipo equipo = new Equipo();
		equipo.setNombreDelProyecto(condicion.getNombreDelProyecto());
		
		//ARRAYLIST DE PERSONAS
		ArrayList<Persona> lideres = sistema.getPersonasPorRol(Roles.Lider);
		ArrayList<Persona> arquitectos = sistema.getPersonasPorRol(Roles.Arquitecto);
		ArrayList<Persona> programadores = sistema.getPersonasPorRol(Roles.Programador);
		ArrayList<Persona> testers = sistema.getPersonasPorRol(Roles.Tester);
		
		//MEZCLA ARREGLOS
		Collections.shuffle(lideres);
		Collections.shuffle(arquitectos);
		Collections.shuffle(programadores);
		Collections.shuffle(testers);
		
		//CALCULA PERSONAS QUE SOBRAN EN CADA GRUPO
		int lideresSobrantes = lideres.size()-condicion.getMaxLideres();
		if (lideresSobrantes<0) lideresSobrantes = 0;
		
		int arquitectosSobrantes = arquitectos.size()-condicion.getMaxArquitectos();
		if (arquitectosSobrantes<0) arquitectosSobrantes = 0;
		
		int programadoresSobrantes = programadores.size()-condicion.getMaxProgramadores();
		if (programadoresSobrantes<0) programadoresSobrantes = 0;
		
		int testersSobrantes = testers.size()-condicion.getMaxTesters();
		if (testersSobrantes<0) testersSobrantes = 0;
		
		//ELIMINA PERSONAS SOBRANTES DE LOS EQUIPOS
		Iterator<Persona> itLideres = lideres.iterator();
		Iterator<Persona> itArquitectos = arquitectos.iterator();
		Iterator<Persona> itProgramadores = programadores.iterator();
		Iterator<Persona> itTesters = testers.iterator();
		
		while (itLideres.hasNext() && lideresSobrantes>0) {
			Persona lider = itLideres.next();
			itLideres.remove();
			lideresSobrantes--;
		}
		while (itLideres.hasNext() && arquitectosSobrantes>0) {
			Persona arquitecto = itArquitectos.next();
			itArquitectos.remove();
			arquitectosSobrantes--;
		}
		while (itProgramadores.hasNext() && programadoresSobrantes>0) {
			Persona programador = itProgramadores.next();
			itProgramadores.remove();
			programadoresSobrantes--;
		}
		while (itTesters.hasNext() && testersSobrantes>0) {
			Persona tester = itTesters.next();
			itTesters.remove();
			testersSobrantes--;
		}
		
		//AGREGA PERSONAS A EQUIPO
		integrantes.addAll(lideres);
		integrantes.addAll(arquitectos);
		integrantes.addAll(programadores);
		integrantes.addAll(testers);
		
		//REEMPLAZA PERSONAS QUE TENGAN EQUIPO SI SE PUEDE
		integrantes = reemplazarPersonasQueTenganEquipo(sistema, integrantes);
		for (Persona integrante: integrantes) {
			equipo.agregarIntegrantePorID(integrante.getId());
		}
		
		//REEMPLAZA PERSONAS INCOMPATIBLES
		while (sistema.equipoTieneIncompatibles(equipo)) {
			integrantes = reemplazarPersonasQueTenganConflictos(sistema, integrantes);
			//AGREGA IDS AL EQUIPO
			equipo.vaciarIntegrantes();
			for (Persona integrante: integrantes) {
				equipo.agregarIntegrantePorID(integrante.getId());
			}
		}
			
		return equipo;
	}
	
	public static ArrayList<Persona> reemplazarPersonasQueTenganEquipo(Sistema sistema, ArrayList<Persona> personasDelGrupo) {
		ArrayList<Persona> integrantes = personasDelGrupo;
		Iterator<Persona> itPersonas = integrantes.iterator();
		while (itPersonas.hasNext()) {
			Persona persona = (Persona) itPersonas.next();
			if (sistema.personaEstaEnAlgunEquipo(persona)) {
				switch (persona.getRol()) {
					case Lider:
						for (Persona lider: sistema.getPersonasPorRol(Roles.Lider)) {
							if (!sistema.personaEstaEnAlgunEquipo(lider)) {
								persona = lider;
								break;
							}
						}
						break;
					case Arquitecto:
						for (Persona arquitecto: sistema.getPersonasPorRol(Roles.Arquitecto)) {
							if (!sistema.personaEstaEnAlgunEquipo(arquitecto)) {
								persona = arquitecto;
								break;
							}
						}
						break;
					case Programador:
						for (Persona programador: sistema.getPersonasPorRol(Roles.Programador)) {
							if (!sistema.personaEstaEnAlgunEquipo(programador)) {
								persona = programador;
								break;
							}
						}
						break;
					case Tester:
						for (Persona tester: sistema.getPersonasPorRol(Roles.Tester)) {
							if (!sistema.personaEstaEnAlgunEquipo(tester)) {
								persona = tester;
								break;
							}
						}
						break;
				}
			}
		}
		return integrantes;
	}
	
	public static ArrayList<Persona> reemplazarPersonasQueTenganConflictos(Sistema sistema, ArrayList<Persona> personasDelGrupo) {
		ArrayList<Persona> integrantes = personasDelGrupo;
		Iterator<Persona> itPersonas = integrantes.iterator();
		while (itPersonas.hasNext()) {
			boolean personaTieneConflicto = false;
			Persona persona = (Persona) itPersonas.next();
			for (Persona personaEnEquipo: integrantes) {
				if (sistema.existeConflictoEntreEstos2Ids(persona.getId(), personaEnEquipo.getId()) && persona.getId()!=personaEnEquipo.getId()) {
					personaTieneConflicto = true;
				}
			}
			if (personaTieneConflicto) {
				itPersonas.remove();
				for (Persona personaNueva: sistema.getPersonas()) {
					if (!integrantes.contains(personaNueva) && personaNueva.getRol() == persona.getRol()) {
						persona = personaNueva;
					}
				}
			}
			
		}
		return integrantes;
	}
	
	//HEURISTICO
	public static Equipo generarEquipoSegundaVersion(Sistema sistema, CondicionesEquipo condicion) {
		Equipo equipo = new Equipo();
		equipo.setNombreDelProyecto(condicion.getNombreDelProyecto());
		
		for (Roles rol: Roles.values()) {
			int randPersonas = 0;
			Persona personaRandomPorRol = new Persona();
			switch (rol) {
				case Lider:
					randPersonas = condicion.getRandLideres();
					break;
				case Arquitecto:
					randPersonas = condicion.getRandArquitectos();
					break;
				case Programador:
					randPersonas = condicion.getRandProgramadores();
					break;
				case Tester:
					randPersonas = condicion.getRandTesters();
					break;
			}
			
			for (int i = 0; i<randPersonas; i++) {
				personaRandomPorRol = sistema.getPersonaRandomPorRol(rol);
				int maxIntentos = sistema.getPersonas().size();
				boolean personaSirve = false;
				while (!personaSirve && maxIntentos>0) {
					//SI EL EQUIPO NO CONTIENE SU ID, LO AGREGA
					if (!equipo.getIntegrantes().contains(personaRandomPorRol.getId())) {
						if (!sistema.idPersonaEsIncompatibleConAlgunIdDelEquipo(personaRandomPorRol.getId(), equipo)) {
							equipo.agregarIntegrantePorID(personaRandomPorRol.getId());
							personaSirve = true;
						}
					}
					maxIntentos--;
				}
			}
		}
		return equipo;
	}

}

