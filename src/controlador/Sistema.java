package controlador;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import modelo.CondicionesEquipo;
import modelo.Equipo;
import modelo.ParesIncompatibles;
import modelo.Persona;
import modelo.Roles;

public class Sistema {

	private ArrayList<Equipo> equiposDeTrabajo;
	private ArrayList<CondicionesEquipo> condicionesParaEquipos;
	private Roles roles;
	private ArrayList<Persona> personas;
	private ArrayList<ParesIncompatibles> listaIncompatibles;

	public Sistema() {
		equiposDeTrabajo = new ArrayList<>();
		condicionesParaEquipos = new ArrayList<>();
		personas = new ArrayList<>();
		listaIncompatibles = new ArrayList<>();
	}

	// ******************
	// * FUNCIONES JSON *
	// ******************

	public void guardarDatosJSON() {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(this);

		try {
			FileWriter writer = new FileWriter("sistema.json");
			writer.write(json);
			writer.close();
		} catch (Exception e) {

		}
	}

	public void cargarDatosJSON() {
		Gson gson = new Gson();
		Sistema sistemaCargar = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(
					"sistema.json"));
			sistemaCargar = gson.fromJson(br, Sistema.class);
		} catch (Exception e) {

		}
		setEquiposDeTrabajo(sistemaCargar.getEquiposDeTrabajo());
		setPersonas(sistemaCargar.getPersonas());
		setListaIncompatibles(sistemaCargar.getListaIncompatibles());
		setCondicionesParaEquipos(sistemaCargar.getCondicionesParaEquipos());
	}

	// *********************
	// * FUNCIONES AGREGAR *
	// *********************

	public void agregarEquipo(Equipo equipo) {
		boolean yaExiste = false;
		for (Equipo equipoAux : equiposDeTrabajo) {
			if (equipo.getNombreDelProyecto().equalsIgnoreCase(
					equipoAux.getNombreDelProyecto())) {
				yaExiste = true;
			}
		}
		if (yaExiste) {
			eliminarEquipoPorNombre(equipo.getNombreDelProyecto());
		}
		equiposDeTrabajo.add(equipo);
	}

	public void agregarCondicion(CondicionesEquipo condicion) {
		boolean existe = false;
		boolean valido = true;
		for (CondicionesEquipo unaCondicion : condicionesParaEquipos) {
			if (unaCondicion.getNombreDelProyecto().equalsIgnoreCase(
					condicion.getNombreDelProyecto())) {
				existe = true;
			}
		}
		if (condicion.getMaxLideres() < condicion.getMinLideres()
				|| condicion.getMaxArquitectos() < condicion
						.getMinArquitectos()
				|| condicion.getMaxProgramadores() < condicion
						.getMinProgramadores()
				|| condicion.getMaxTesters() < condicion.getMinTesters()) {
			valido = false;
		}
		if (existe) {
			throw new RuntimeException("Ese nombre ya se encuentra registrado.");
		} else if (!valido) {
			throw new RuntimeException(
					"Los valores m�ximos no pueden ser menores a los m�nimos.");
		} else {
			condicionesParaEquipos.add(condicion);
		}

	}

	public void agregarPersona(Persona persona) {
		if (!FuncionesStrings.soloLetras(persona.getNombre())) {
			throw new RuntimeException(persona.getNombre()
					+ ": No es un nombre valido.");
		} else if (nombreDePersonaYaExiste(persona.getNombre())) {
			throw new RuntimeException("Esa persona ya se encuentra guardada.");
		} else if (persona.getNombre().equals("")) {
			throw new RuntimeException("Ingrese el nombre de la persona.");
		} else {
			persona.setId(getMaxIDPersona() + 1);
			personas.add(persona);
		}
	}

	public void agregarParIncompatible(ParesIncompatibles par) {
		boolean existe = false;
		for (ParesIncompatibles unPar : listaIncompatibles) {
			if (unPar.equals(par)) {
				existe = true;
			}
		}

		if (existe) {
			throw new RuntimeException(
					"Esa incompatibilidad ya se encuentra registrada");
		} else {
			listaIncompatibles.add(par);
		}

	}

	// **************************
	// * GETTERS PERSONALIZADOS *
	// **************************

	public Persona getPersonaPorId(int id) {
		for (Persona pers : personas) {
			if (pers.getId() == id) {
				return pers;
			}
		}
		return null;
	}

	public Equipo getEquipoPorNombre(String nombre) {
		for (Equipo equipo : equiposDeTrabajo) {
			if (equipo.getNombreDelProyecto().equalsIgnoreCase(nombre)) {
				return equipo;
			}
		}
		return null;
	}

	public CondicionesEquipo getCondicionPorNombre(String nombre) {
		for (CondicionesEquipo cond : condicionesParaEquipos) {
			if (cond.getNombreDelProyecto().equalsIgnoreCase(nombre)) {
				return cond;
			}
		}
		return null;
	}

	public ArrayList<Persona> getPersonasPorRol(Roles rol) {
		Stream<Persona> streamDePersonas = personas.stream();
		return streamDePersonas.filter(x -> x.getRol() == rol).collect(
				Collectors.toCollection(ArrayList::new));
	}

	// ***********************
	// * FUNCIONES MODIFICAR *
	// ***********************

	public void actualizarPersona(Persona persona) {
		Iterator<Persona> it = personas.iterator();
		while (it.hasNext()) {
			Persona pers = it.next();
			if (pers.getId() == persona.getId()) {
				pers.setRol(persona.getRol());
				pers.setNombre(persona.getNombre());
			}
		}
	}

	public boolean actualizarCondicion(CondicionesEquipo cond) {
		Iterator<CondicionesEquipo> it = condicionesParaEquipos.iterator();
		boolean valido = true;
		while (it.hasNext()) {
			CondicionesEquipo condIt = it.next();
			if (condIt.getNombreDelProyecto().equalsIgnoreCase(
					cond.getNombreDelProyecto())) {
				if (cond.getMaxLideres() < cond.getMinLideres()
						|| cond.getMaxArquitectos() < cond.getMinArquitectos()
						|| cond.getMaxProgramadores() < cond
								.getMinProgramadores()
						|| cond.getMaxTesters() < cond.getMinTesters()) {
					valido = false;
				}
				condIt.setMinArquitectos(cond.getMinArquitectos());
				condIt.setMaxArquitectos(cond.getMaxArquitectos());
				condIt.setMinLideres(cond.getMinLideres());
				condIt.setMaxLideres(cond.getMaxLideres());
				condIt.setMaxProgramadores(cond.getMaxProgramadores());
				condIt.setMinProgramadores(cond.getMinProgramadores());
				condIt.setMinTesters(cond.getMinTesters());
				condIt.setMaxTesters(cond.getMaxTesters());
			}
		}
		if (valido) {
			return true;
		} else {
			throw new RuntimeException(
					"Los valores m�ximos no pueden ser menores a los m�nimos.");
		}

	}

	// **********************
	// * FUNCIONES ELIMINAR *
	// **********************

	public void eliminarEquipoPorNombre(String nomb) {
		Iterator<Equipo> it = equiposDeTrabajo.iterator();
		while (it.hasNext()) {
			Equipo eq = it.next();
			if (eq.getNombreDelProyecto().equalsIgnoreCase(nomb)) {
				it.remove();
			}
		}
	}

	public void eliminarCondicionPorNombre(String nombre) {
		Iterator<CondicionesEquipo> it = condicionesParaEquipos.iterator();
		while (it.hasNext()) {
			CondicionesEquipo cond = it.next();
			if (cond.getNombreDelProyecto().equalsIgnoreCase(nombre)) {
				it.remove();
			}
		}
	}

	public void eliminarPersonaPorId(int id) {
		Iterator<Persona> it = personas.iterator();
		while (it.hasNext()) {
			Persona pers = it.next();
			if (pers.getId() == id) {
				it.remove();
			}
		}

		// BORRA FOTO
		File fichero = new File("fotos/" + Integer.toString(id) + ".jpg");
		if (fichero.exists()) {
			fichero.delete();
		}

		// SI PERSONA PERTENECE A UN GRUPO, LA SACA DEL MISMO
		Iterator<Equipo> it2 = equiposDeTrabajo.iterator();
		while (it2.hasNext()) {
			Equipo eq = it2.next();
			if (eq.getIntegrantes().contains(id)) {
				eq.getIntegrantes().remove(id);
			}
		}
	}

	public void eliminarIncompatibilidadPorStringNombre(String nombres) {
		String[] arrAux = nombres.split(" - ");
		int id1 = getIDPersonaPorNombre(arrAux[0]);
		int id2 = getIDPersonaPorNombre(arrAux[1]);

		Iterator<ParesIncompatibles> it = listaIncompatibles.iterator();
		while (it.hasNext()) {
			ParesIncompatibles par = it.next();
			if (par.getId_1() == id1 && par.getId_2() == id2) {
				it.remove();
			}
		}
	}

	// ************************
	// * FUNCIONES AUXILIARES *
	// ************************
	public boolean personaEstaEnAlgunEquipo(Persona persona) {
		for (Equipo equipo : equiposDeTrabajo) {
			if (equipo.getIntegrantes().contains(persona.getId())) {
				return true;
			}
		}
		return false;
	}

	public int getMaxIDPersona() {
		int maxId = 0;
		if (personas.size() == 0) {
			return 0;
		} else {
			maxId = personas.get(0).getId();
			for (Persona p : personas) {
				if (p.getId() > maxId) {
					maxId = p.getId();
				}
			}
		}
		return maxId;
	}

	public int getIDPersonaPorNombre(String nombre) {
		for (Persona persona : personas) {
			if (persona.getNombre().equalsIgnoreCase(nombre)) {
				return persona.getId();
			}
		}
		return 0;
	}

	public boolean nombreDePersonaYaExiste(String nombre) {
		boolean existe = false;
		for (Persona persona : personas) {
			if (persona.getNombre().equalsIgnoreCase(nombre)) {
				existe = true;
			}
		}
		return existe;
	}

	public ArrayList<String> getArrayNombreGruposAlQuePerteneceLaPersona(
			int idPersona) {
		ArrayList<String> grupos = new ArrayList<>();
		for (Equipo equipo : equiposDeTrabajo) {
			if (equipo.getIntegrantes().contains(idPersona)) {
				grupos.add(equipo.getNombreDelProyecto());
			}
		}
		return grupos;
	}

	public String getDatosEquipo(Equipo equipo) {
		int lideres = 0;
		int arquitectos = 0;
		int programadores = 0;
		int testers = 0;

		String datos = "";
		for (Integer id : equipo.getIntegrantes()) {
			Persona persona = getPersonaPorId(id);
			if (persona.getRol() == Roles.Lider) {
				lideres++;
			}
			if (persona.getRol() == Roles.Arquitecto) {
				arquitectos++;
			}
			if (persona.getRol() == Roles.Programador) {
				programadores++;
			}
			if (persona.getRol() == Roles.Tester) {
				testers++;
			}
		}
		datos += "L�deres: " + lideres + "\n";
		datos += "Arquitectos: " + arquitectos + "\n";
		datos += "Programadores: " + programadores + "\n";
		datos += "Testers: " + testers;

		return datos;
	}

	public boolean existeConflictoEntreEstos2Ids(int id1, int id2) {
		boolean ret = false;
		for (ParesIncompatibles par : listaIncompatibles) {
			if (par.contieneIds(id1, id2)) {
				ret = true;
			}
		}
		return ret;
	}

	// ******************************
	// * FUNCION PARA ARMAR EQUIPOS *
	// ******************************

	public boolean equipoTieneIncompatibles(Equipo equipo) {
		boolean tieneIncompatibles = false;
		for (Integer id1 : equipo.getIntegrantes()) {
			for (Integer id2 : equipo.getIntegrantes()) {
				if (existeConflictoEntreEstos2Ids(id1, id2) && id1 != id2) {
					tieneIncompatibles = true;
				}
			}
		}
		return tieneIncompatibles;
	}

	public Persona getPersonaRandomPorRol(Roles rol) {
		Random rand = new Random();
		Stream<Persona> streamDePersonas = personas.stream();
		ArrayList<Persona> personasFiltradas = streamDePersonas.filter(
				x -> x.getRol() == rol).collect(
				Collectors.toCollection(ArrayList::new));
		int index = rand.nextInt(personasFiltradas.size());
		return personasFiltradas.get(index);
	}

	public boolean idPersonaEsIncompatibleConAlgunIdDelEquipo(int id,
			Equipo equipo) {
		boolean esIncompatible = false;
		for (Integer id1 : equipo.getIntegrantes()) {
			for (ParesIncompatibles par : listaIncompatibles) {
				if ((par.getId_1() == id1 || par.getId_2() == id1)
						&& (par.getId_2() == id || par.getId_1() == id)) {
					esIncompatible = true;
				}
			}
		}
		return esIncompatible;
	}

	public boolean equipoCumpleCondicion(Equipo equipo,
			CondicionesEquipo condicion) {
		boolean cumple = true;
		int cantLideres = 0;
		int cantArquitectos = 0;
		int cantProgramadores = 0;
		int cantTesters = 0;

		for (Integer id : equipo.getIntegrantes()) {
			Persona persona = getPersonaPorId(id);
			switch (persona.getRol()) {
			case Lider:
				cantLideres++;
				break;
			case Arquitecto:
				cantArquitectos++;
				break;
			case Programador:
				cantProgramadores++;
				break;
			case Tester:
				cantTesters++;
				break;
			}
		}

		if (cantLideres < condicion.getMinLideres()
				|| cantLideres > condicion.getMaxLideres()) {
			cumple = false;
		}
		if (cantArquitectos < condicion.getMinArquitectos()
				|| cantArquitectos > condicion.getMaxArquitectos()) {
			cumple = false;
		}
		if (cantProgramadores < condicion.getMinProgramadores()
				|| cantProgramadores > condicion.getMaxProgramadores()) {
			cumple = false;
		}
		if (cantTesters < condicion.getMinTesters()
				|| cantTesters > condicion.getMaxTesters()) {
			cumple = false;
		}

		return cumple;
	}

	// *********************
	// * GETTERS Y SETTERS *
	// *********************

	public ArrayList<Equipo> getEquiposDeTrabajo() {
		return equiposDeTrabajo;
	}

	public void setEquiposDeTrabajo(ArrayList<Equipo> equiposDeTrabajo) {
		this.equiposDeTrabajo = equiposDeTrabajo;
	}

	public ArrayList<CondicionesEquipo> getCondicionesParaEquipos() {
		return condicionesParaEquipos;
	}

	public void setCondicionesParaEquipos(
			ArrayList<CondicionesEquipo> condicionesParaEquipos) {
		this.condicionesParaEquipos = condicionesParaEquipos;
	}

	public Roles getRoles() {
		return roles;
	}

	public void setRoles(Roles roles) {
		this.roles = roles;
	}

	public ArrayList<Persona> getPersonas() {
		return personas;
	}

	public void setPersonas(ArrayList<Persona> personas) {
		this.personas = personas;
	}

	public ArrayList<ParesIncompatibles> getListaIncompatibles() {
		return listaIncompatibles;
	}

	public void setListaIncompatibles(
			ArrayList<ParesIncompatibles> listaIncompatibles) {
		this.listaIncompatibles = listaIncompatibles;
	}

	// PRUEBA
	public void agregarDatosDePrueba() {
		// AGREGA 14 PERSONAS DE PRUEBA
		personas.add(new Persona(1, "Mart�n Gonzalez ", Roles.Lider));// ID 1
		personas.add(new Persona(2, "C�sar Cruz", Roles.Lider));// ID 2
		personas.add(new Persona(3, "Noelia Gimenez", Roles.Lider));// ID 3
		personas.add(new Persona(4, "�rik Costa", Roles.Lider));// ID 4
		personas.add(new Persona(5, "Alejandro Riera", Roles.Lider));// ID 5
		personas.add(new Persona(6, "Clara Lozano", Roles.Lider));// ID 6

		personas.add(new Persona(7, "Victoria Flores", Roles.Arquitecto));// ID
																			// 7
		personas.add(new Persona(8, "Pedro Aguilar", Roles.Arquitecto));// ID 8
		personas.add(new Persona(9, "Daniel Fernandez", Roles.Arquitecto));// ID
																			// 9
		personas.add(new Persona(10, "Santiago Nu�ez", Roles.Arquitecto));// ID
																			// 10
		personas.add(new Persona(11, "Antonio Sala", Roles.Arquitecto));// ID 11
		personas.add(new Persona(12, "Roc�o Sanz", Roles.Arquitecto));// ID 12
		personas.add(new Persona(13, "Jos� Fernandez", Roles.Arquitecto));// ID
																			// 13
		personas.add(new Persona(14, "Gerard Moya", Roles.Arquitecto));// ID 14
		personas.add(new Persona(15, "Noelia Calvo", Roles.Arquitecto));// ID 15

		personas.add(new Persona(16, "Aaron Martinez", Roles.Programador));// ID
																			// 16
		personas.add(new Persona(17, "Adria Segura", Roles.Programador));// ID
																			// 17
		personas.add(new Persona(18, "Adri�n Garrido", Roles.Programador));// ID
																			// 18
		personas.add(new Persona(19, "Daniela Sanz", Roles.Programador));// ID
																			// 19
		personas.add(new Persona(20, "Jes�s Campos", Roles.Programador));// ID
																			// 20
		personas.add(new Persona(21, "Alicia Segura", Roles.Programador));// ID
																			// 21
		personas.add(new Persona(22, "Iv�n Diaz", Roles.Programador));// ID 22
		personas.add(new Persona(23, "Francisco Cano", Roles.Programador));// ID
																			// 23
		personas.add(new Persona(24, "Nadia Caballero", Roles.Programador));// ID
																			// 24
		personas.add(new Persona(25, "Omar Hidalgo", Roles.Programador));// ID
																			// 25
		personas.add(new Persona(26, "Helena Castillo", Roles.Programador));// ID
																			// 26
		personas.add(new Persona(27, "Willfredo", Roles.Programador));// ID 27
		personas.add(new Persona(28, "Eduardo Herrera", Roles.Programador));// ID
																			// 28
		personas.add(new Persona(29, "Diana Hernandez", Roles.Programador));// ID
																			// 29
		personas.add(new Persona(30, "Carla Mora", Roles.Programador));// ID 30
		personas.add(new Persona(31, "Aitor Calvo", Roles.Programador));// ID 31
		personas.add(new Persona(32, "Juan Jos� Pons", Roles.Programador));// ID
																			// 32
		personas.add(new Persona(33, "Cristina Carrasco", Roles.Programador));// ID
																				// 33
		personas.add(new Persona(34, "Sandra Carmona", Roles.Programador));// ID
																			// 34
		personas.add(new Persona(35, "Gerard Sole", Roles.Programador));// ID 35

		personas.add(new Persona(36, "F�tima Rovira", Roles.Tester));// ID 36
		personas.add(new Persona(37, "Jon Romero", Roles.Tester));// ID 37
		personas.add(new Persona(38, "Blanca Marquez", Roles.Tester));// ID 38
		personas.add(new Persona(39, "Hugo Aguilar", Roles.Tester));// ID 39
		personas.add(new Persona(40, "Nadia Nu�ez", Roles.Tester));// ID 40
		personas.add(new Persona(41, "�scar Blanco", Roles.Tester));// ID 41
		personas.add(new Persona(42, "Ariadna Carrasco", Roles.Tester));// ID 42
		personas.add(new Persona(43, "Luis Pons", Roles.Tester));// ID 43
		personas.add(new Persona(44, "Andrea Vargas", Roles.Tester));// ID 44
		personas.add(new Persona(45, "Alonso Ramos", Roles.Tester));// ID 45
		personas.add(new Persona(46, "Iris Delgado", Roles.Tester));// ID 46
		personas.add(new Persona(47, "Julia Rodriguez", Roles.Tester));// ID 47
		personas.add(new Persona(48, "Naia Iglesias", Roles.Tester));// ID 48
		personas.add(new Persona(49, "Daniel Castillo", Roles.Tester));// ID 49
		personas.add(new Persona(50, "Ismael Hidalgo", Roles.Tester));// ID 50
		personas.add(new Persona(51, "Salma Castillo ", Roles.Tester));// ID 51
		personas.add(new Persona(52, "�ngela Vazquez", Roles.Tester));// ID 52
		personas.add(new Persona(53, "Andr�s Vazquez", Roles.Tester));// ID 53
		personas.add(new Persona(54, "Valentina Medina", Roles.Tester));// ID 54
		personas.add(new Persona(55, "Carla Flores", Roles.Tester));// ID 55

		// AGREGA 1 PAR INCOMPATIBLE
		ParesIncompatibles par1 = new ParesIncompatibles(1, 9);
		ParesIncompatibles par2 = new ParesIncompatibles(12, 34);
		ParesIncompatibles par3 = new ParesIncompatibles(22, 3);
		ParesIncompatibles par4 = new ParesIncompatibles(50, 8);
		ParesIncompatibles par5 = new ParesIncompatibles(12, 37);
		listaIncompatibles.add(par1);
		listaIncompatibles.add(par2);
		listaIncompatibles.add(par3);
		listaIncompatibles.add(par4);
		listaIncompatibles.add(par5);

		// AGREGA 1 CONDICION
		CondicionesEquipo cond1 = new CondicionesEquipo();
		cond1.setNombreDelProyecto("PROYECTO TRES");
		cond1.setIdeaDelProyecto("A DESARROLLAR");
		cond1.setMinLideres(1);
		cond1.setMaxLideres(1);
		cond1.setMinArquitectos(2);
		cond1.setMaxArquitectos(3);
		cond1.setMinProgramadores(6);
		cond1.setMaxProgramadores(8);
		cond1.setMinTesters(3);
		cond1.setMaxTesters(4);

		condicionesParaEquipos.add(cond1);
		guardarDatosJSON();
	}

}
