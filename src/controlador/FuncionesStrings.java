package controlador;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FuncionesStrings {

	public static String capitalize(String str) {
        boolean prevWasWhiteSp = true;
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (Character.isLetter(chars[i])) {
                if (prevWasWhiteSp) {
                    chars[i] = Character.toUpperCase(chars[i]);    
                }
                prevWasWhiteSp = false;
        } else {
            prevWasWhiteSp = Character.isWhitespace(chars[i]);
        }
    }
    return new String(chars);
    }
	public static boolean soloLetras(String string){
		Pattern p = Pattern.compile("\\D{1,}");
		Matcher m = p.matcher(string);
		
		if (m.matches()){
			return true;}
		else{
			return false;}
	}
	
}
