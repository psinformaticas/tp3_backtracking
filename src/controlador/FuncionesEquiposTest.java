package controlador;

import static org.junit.Assert.*;

import java.util.ArrayList;

import modelo.CondicionesEquipo;
import modelo.Equipo;
import modelo.Persona;
import modelo.Roles;

import org.junit.Before;
import org.junit.Test;

public class FuncionesEquiposTest {
	Contenedor contenedor;
	CondicionesEquipo condicion;
	Equipo equipo;
	Equipo equipoAux;
	ArrayList<Persona> personas;
	ArrayList<Persona> personasAux;
	
	@Before
	public void inicializar(){
	contenedor = new Contenedor();
	personas= new ArrayList<>();
	personasAux= new ArrayList<>();
	Sistema sistema = new Sistema();
	equipo = new Equipo();
	equipoAux= new Equipo();
	
	equipo.setNombreDelProyecto("TEST");
	
	condicion = new CondicionesEquipo();
	condicion.setNombreDelProyecto("TEST");
	condicion.setMinLideres(1);
	condicion.setMaxLideres(1);
	condicion.setMinArquitectos(2);
	condicion.setMaxArquitectos(2);
	condicion.setMinProgramadores(3);
	condicion.setMaxProgramadores(3);	
	condicion.setMinTesters(2);
	condicion.setMaxTesters(2);
	
	Persona p1= new Persona(1,"Marcos Rodriguez", Roles.Lider);
	Persona p2= new Persona(2, "Marta Lopez", Roles.Arquitecto);
	Persona p3= new Persona(3,"Victoria Ocampo", Roles.Arquitecto);
	Persona p4= new Persona(4, "Luis Perez", Roles.Programador);
	Persona p5= new Persona(5, "Jorge Pi�ero", Roles.Programador);
	Persona p6= new Persona(6, "Mariana Puchini", Roles.Programador);
	Persona p7= new Persona(7,"Adriana Casas", Roles.Tester);
	Persona p8= new Persona(8, "Fabian Arias", Roles.Tester);
	Persona p9= new Persona(9, "Cristial Cardozo", Roles.Tester);
	
	personas.add(p1);
	personas.add(p2);
	personas.add(p3);
	personas.add(p4);
	personas.add(p5);
	personas.add(p6);
	personas.add(p7);
	personas.add(p8);
	
	
	personasAux.add(p1);
	personasAux.add(p2);
	personasAux.add(p3);
	personasAux.add(p4);
	personasAux.add(p5);
	personasAux.add(p6);
	personasAux.add(p7);
	personasAux.add(p9);
	
	equipoAux.agregarIntegrantePorID(p8.getId());
	
	for (Persona p: personas) {
		equipo.agregarIntegrantePorID(p.getId());
		contenedor.getSistema().agregarPersona(p);
	}
	personas.add(p9);
	contenedor.getSistema().agregarCondicion(condicion);
	}
	
	
	@Test
	public void generarEquipoTest() {
		assertEquals(FuncionesEquipos.generarEquipo(contenedor.getSistema(), condicion), equipo);
	}
	@Test
	public void reemplazarPersonasQueTenganEquipoTest(){
		assertEquals(FuncionesEquipos.reemplazarPersonasQueTenganEquipo(contenedor.getSistema(), personas), personasAux);
	}

}
