package controlador;

import modelo.CondicionesEquipo;
import modelo.Equipo;
import modelo.Roles;

public class Contenedor {
	
	private Sistema sistema;
	
	public Contenedor() {
		sistema = new Sistema();
	}

	public Sistema getSistema() {
		return sistema;
	}

	public void setSistema(Sistema sistema) {
		this.sistema = sistema;
	}
	
	public void generarEquipo(CondicionesEquipo condicion) {
		if (sistema.getPersonasPorRol(Roles.Lider).size()<condicion.getMinLideres() || sistema.getPersonasPorRol(Roles.Arquitecto).size()<condicion.getMinArquitectos() || sistema.getPersonasPorRol(Roles.Programador).size()<condicion.getMinProgramadores() || sistema.getPersonasPorRol(Roles.Tester).size()<condicion.getMinTesters()) {
			throw new RuntimeException("El sistema no cuenta con la cantidad m�nima de personas registradas para formar ese equipo.");
		}
		boolean equipoCumpleCondicion = false;
		Equipo equipo = new Equipo();
		while (!equipoCumpleCondicion) {
			equipo = FuncionesEquipos.generarEquipo(sistema, condicion);
			if (sistema.equipoCumpleCondicion(equipo, condicion)) {
				equipoCumpleCondicion = true;
			}
		}
		sistema.agregarEquipo(equipo);
	}
	
	public void generarEquipoSegundaVersion(CondicionesEquipo condicion) {
		if (sistema.getPersonasPorRol(Roles.Lider).size()<condicion.getMinLideres() || sistema.getPersonasPorRol(Roles.Arquitecto).size()<condicion.getMinArquitectos() || sistema.getPersonasPorRol(Roles.Programador).size()<condicion.getMinProgramadores() || sistema.getPersonasPorRol(Roles.Tester).size()<condicion.getMinTesters()) {
			throw new RuntimeException("El sistema no cuenta con la cantidad m�nima de personas registradas para formar ese equipo.");
		}
		boolean equipoCumpleCondicion = false;
		Equipo equipo = new Equipo();
		while (!equipoCumpleCondicion) {
			equipo = FuncionesEquipos.generarEquipoSegundaVersion(sistema, condicion);
			if (sistema.equipoCumpleCondicion(equipo, condicion)) {
				equipoCumpleCondicion = true;
			}
		}
		sistema.agregarEquipo(equipo);
	}
	
	
}
