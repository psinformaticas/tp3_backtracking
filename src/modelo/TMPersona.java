package modelo;


import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.List;
import controlador.*;
import modelo.Roles;

public class TMPersona implements TableModel {

    private List<Persona> personas;
    private Roles rol;
    
    public TMPersona(List<Persona> lista) {
    	personas = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return personas.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "ID";
                break;
            }
            case 1: {
                titulo = "Nombre";
                break;                
            }
            case 2: {
                titulo = "Rol";
                break;                
            }
            
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex == 0) {
            return Integer.class;
            
        }
        else {
            return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Persona persona = personas.get(rowIndex);
        String valor = null;
        switch(columnIndex) {
            case 0: {
                return persona.getId();
            }
            case 1: {
                valor = FuncionesStrings.capitalize(persona.getNombre());
                break;                
            }
            case 2: {
            	switch (persona.getRol()) {
            	  case Lider:
            	    valor = ("L�der");
            	    break;
            	  case Arquitecto:
            		valor = ("Arquitecto");
            	    break;
            	  case Programador:
            		valor = ("Programador");
            	    break;
            	  case Tester:
            		valor = ("Tester");
              	    break;
            	}
                break;                
            }
        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

