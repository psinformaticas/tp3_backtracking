package modelo;

public class CondicionesEquipo {
	String nombreDelProyecto, ideaDelProyecto;
	int minLideres, maxLideres, minArquitectos, maxArquitectos, minProgramadores, 
	maxProgramadores, minTesters, maxTesters;
	
	
	

	public CondicionesEquipo() {
		this.nombreDelProyecto = "";
		this.ideaDelProyecto = "";
		this.minLideres = 0;
		this.maxLideres = 0;
		this.minArquitectos = 0;
		this.maxArquitectos = 0;
		this.minProgramadores = 0;
		this.maxProgramadores = 0;
		this.minTesters = 0;
		this.maxTesters = 0;
	}
	
	public CondicionesEquipo(String nombreDelProyecto,String ideaDelProyecto, int minLideres, int maxLideres , 
							int minArq, int maxArq, int minProgramadores,int maxProgramadores, int minTesters, 
							int maxTesters) {
		
		this.nombreDelProyecto = nombreDelProyecto;
		this.ideaDelProyecto = ideaDelProyecto;
		this.minLideres = minLideres;
		this.maxLideres = maxLideres;
		this.minArquitectos = minArq;
		this.maxArquitectos = maxArq;
		this.minProgramadores = minProgramadores;
		this.maxProgramadores = maxProgramadores;
		this.minTesters = minTesters;
		this.maxTesters = maxTesters;
	}

	public String getNombreDelProyecto() {
		return nombreDelProyecto;
	}

	public void setNombreDelProyecto(String nombreDelProyecto) {
		this.nombreDelProyecto = nombreDelProyecto;
	}

	public String getIdeaDelProyecto() {
		return ideaDelProyecto;
	}

	public void setIdeaDelProyecto(String ideaDelProyecto) {
		this.ideaDelProyecto = ideaDelProyecto;
	}

	
	@Override
	public boolean equals(Object obj) {
		boolean ret = true;
		if (obj == null) {
			ret = false;
		}
		if (getClass() != obj.getClass()) {
			ret = false;
		}
		
		if (ret) {
			CondicionesEquipo condicionesEquipo2= (CondicionesEquipo) obj;
			if(nombreDelProyecto.equalsIgnoreCase(condicionesEquipo2.getNombreDelProyecto())){
				return false;
			}
		}
		return true;
	}

	public int getMinLideres() {
		return minLideres;
	}

	public void setMinLideres(int minLideres) {
		this.minLideres = minLideres;
	}

	public int getMaxLideres() {
		return maxLideres;
	}
	
	public int getRandLideres() {
		return (int)(Math.random()*(maxLideres-minLideres+1)+minLideres);
	}
	
	public void setMaxLideres(int maxLideres) {
		this.maxLideres = maxLideres;
	}

	public int getMinArquitectos() {
		return minArquitectos;
	}

	public void setMinArquitectos(int minArquitectos) {
		this.minArquitectos = minArquitectos;
	}

	public int getRandArquitectos() {
		return (int)(Math.random()*(maxArquitectos-minArquitectos+1)+minArquitectos);
	}
	
	public int getMaxArquitectos() {
		return maxArquitectos;
	}

	public void setMaxArquitectos(int maxArquitectos) {
		this.maxArquitectos = maxArquitectos;
	}

	public int getMinProgramadores() {
		return minProgramadores;
	}

	public void setMinProgramadores(int minProgramadores) {
		this.minProgramadores = minProgramadores;
	}

	public int getMaxProgramadores() {
		return maxProgramadores;
	}

	public int getRandProgramadores() {
		return (int)(Math.random()*(maxProgramadores-minProgramadores+1)+minProgramadores);
	}
	
	public void setMaxProgramadores(int maxProgramadores) {
		this.maxProgramadores = maxProgramadores;
	}

	public int getMinTesters() {
		return minTesters;
	}

	public void setMinTesters(int minTesters) {
		this.minTesters = minTesters;
	}

	public int getMaxTesters() {
		return maxTesters;
	}
	
	public int getRandTesters() {
		return (int)(Math.random()*(maxTesters-minTesters+1)+minTesters);
	}

	public void setMaxTesters(int maxTesters) {
		this.maxTesters = maxTesters;
	}

//	@Override
//	public String toString() {
//		// TODO Auto-generated method stub
//		return nombreDelProyecto+"-."+ideaDelProyecto+"-."+String.valueOf(cantidadDeLideres)+"-."+String.valueOf(cantidadDeArquitectos)+"-."+
//				String.valueOf(cantidadDeProgramadores)+"-."+String.valueOf(cantidadDeTesters);
//	}
//	
//	public void stringACondiciones(String[] string){
//		nombreDelProyecto=string[0];
//		ideaDelProyecto=string[1];
//		cantidadDeLideres=Integer.parseInt(string[2]);
//		cantidadDeArquitectos=Integer.parseInt(string[3]); 
//		cantidadDeProgramadores=Integer.parseInt(string[4]); 
//		cantidadDeTesters=Integer.parseInt(string[5]);
//	}
}
