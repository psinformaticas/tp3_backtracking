package modelo;

public class ParesIncompatibles {
	private int idPersona_1;
	private int idPersona_2;
	
	public ParesIncompatibles(){
		this.idPersona_1=0;
		this.idPersona_2=0;
	}
	
	public ParesIncompatibles(int persona_1,int persona_2){
		this.idPersona_1=persona_1;
		this.idPersona_2=persona_2;
	}
	public int getId_1(){
		return idPersona_1;
	}
	public int getId_2(){
		return idPersona_2;
	}
	
	public boolean contieneIds(int id1, int id2) {
		boolean losContiene = false;
		if ((idPersona_1 == id1 || idPersona_2 == id1) && (idPersona_1 == id2 || idPersona_2 == id2)) {
			losContiene = true;
		}
		return losContiene;
	}

	@Override
	public boolean equals(Object obj) {
		boolean ret = true;
		if (obj == null) {
			ret = false;
		}
		if (getClass() != obj.getClass()) {
			ret = false;
		}
		
		if (ret) {
			ParesIncompatibles otroPar= (ParesIncompatibles) obj;
			if(idPersona_1 != otroPar.getId_1() || idPersona_2 != otroPar.getId_2()){
				return false;
			}
		}
		return true;
	}


}
