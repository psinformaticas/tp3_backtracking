package modelo;


import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import vista.Principal;

import java.util.List;

import controlador.*;
import modelo.Roles;

public class TMEquipo implements TableModel {

    private List<Equipo> equipos;
    
    public TMEquipo(List<Equipo> lista) {
    	equipos = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return equipos.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Proyecto";
                break;
            }
            case 1: {
                titulo = "Integrantes";
                break;                
            }
            
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
       return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Equipo equipo = equipos.get(rowIndex);
        String valor = null;
        switch(columnIndex) {
            case 0: {
            	valor = FuncionesStrings.capitalize(equipo.getNombreDelProyecto());
            	break;
            }
            case 1: {
            	int cont = 0;
            	valor = "";
            	for (Integer i: equipo.getIntegrantes()) {
            		cont++;
            		if (cont==equipo.getIntegrantes().size()) {
            			valor += FuncionesStrings.capitalize(Principal.contenedor.getSistema().getPersonaPorId(i).getNombre());	
            		} else {
            			valor += FuncionesStrings.capitalize(Principal.contenedor.getSistema().getPersonaPorId(i).getNombre())+", ";
            		}
            		
            	}
                break;                
            }
        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

