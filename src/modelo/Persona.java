package modelo;

import java.util.ArrayList;

public class Persona {
	private int id;
	private String nombre;
	private Roles rol;
	
	public Persona(){
		id=0;
		nombre="";
		rol=null;
	}
	
	public Persona(int id, String nombre, Roles rol) {
		this.id=id;
		this.nombre=nombre;
		this.rol=rol;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setRol(Roles rol) {
		this.rol = rol;
	}
	
	public int getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public Roles getRol() {
		return rol;
	}

	public boolean sonCompatibles (ArrayList<ParesIncompatibles> listaIncompatibles , Persona otraPersona){
		boolean ret= false;
		for(ParesIncompatibles par: listaIncompatibles){
			if(par.getId_1()==id || par.getId_2()==id ){
				ret=ret || (par.getId_1()==otraPersona.getId() || par.getId_2()==otraPersona.getId());
			}
		}
		return ret;
	}

	@Override
	public boolean equals(Object obj) {
		boolean ret = true;
		if (obj == null) {
			ret = false;
		}
		if (getClass() != obj.getClass()) {
			ret = false;
		}
		
		if (ret) {

			Persona otraPersona= (Persona) obj;
			if(id!=otraPersona.getId() || !nombre.equalsIgnoreCase(otraPersona.getNombre())){
				return false;
			}
			
		
		}
		
		return true;
	}

	@Override
	public String toString() {
		return String.valueOf(id)+"-."+nombre+"-."+rol+"-.";
	}
	

}
