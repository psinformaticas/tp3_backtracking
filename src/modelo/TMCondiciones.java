package modelo;


import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import vista.Principal;

import java.util.List;

import controlador.*;
import modelo.Roles;

public class TMCondiciones implements TableModel {

    private List<CondicionesEquipo> condiciones;
    
    public TMCondiciones(List<CondicionesEquipo> lista) {
    	condiciones = lista;
    }
    
    
    @Override
    public int getRowCount() {
        return condiciones.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public String getColumnName(int columnIndex) {
        String titulo = null;
        
        switch(columnIndex) {
            case 0: {
                titulo = "Proyecto";
                break;
            }
            case 1: {
                titulo = "Lideres";
                break;                
            }
            case 2: {
                titulo = "Arquitectos";
                break;                
            }
            case 3: {
                titulo = "Programadores";
                break;                
            }
            case 4: {
                titulo = "Testers";
                break;                
            }
            
        }
        return titulo;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
       return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        CondicionesEquipo condicion = condiciones.get(rowIndex);
        String valor = null;
        switch(columnIndex) {
            case 0: {
            	valor = FuncionesStrings.capitalize(condicion.getNombreDelProyecto());
            	break;
            }
            case 1: {
            	valor = Integer.toString(condicion.getMinLideres())+" a "+Integer.toString(condicion.getMaxLideres());
                break;                
            }
            case 2: {
            	valor = Integer.toString(condicion.getMinArquitectos())+" a "+Integer.toString(condicion.getMaxArquitectos());
                break;                
            }
            case 3: {
            	valor = Integer.toString(condicion.getMinProgramadores())+" a "+Integer.toString(condicion.getMaxProgramadores());
                break;                
            }
            case 4: {
            	valor = Integer.toString(condicion.getMinTesters())+" a "+Integer.toString(condicion.getMaxTesters());
                break;                
            }
        }
        return valor;
    }

    @Override
    public void setValueAt(Object o, int rowIndex, int columnIndex) {
        
    }

    @Override
    public void addTableModelListener(TableModelListener tl) {
        
    }

    @Override
    public void removeTableModelListener(TableModelListener tl) {
        
    }
    
}

