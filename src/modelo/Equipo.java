package modelo;

import java.util.HashSet;
import java.util.Set;

public class Equipo {
	private String nombreDelProyecto;
	private Set<Integer> integrantes;
	
	public Equipo(){
		nombreDelProyecto="";
		integrantes=new HashSet<>();
	}

	
	public void agregarIntegrantePorID(int id) {
		integrantes.add(id);
	}
	
	public String getNombreDelProyecto() {
		return nombreDelProyecto;
	}

	public void setNombreDelProyecto(String nombreDelProyecto) {
		this.nombreDelProyecto = nombreDelProyecto;
	}

	public Set<Integer> getIntegrantes() {
		return integrantes;
	}

	public void setIntegrantes(Set<Integer> integrantes) {
		this.integrantes = integrantes;
	}
	
	public void vaciarIntegrantes() {
		integrantes.clear();
	}
	
	@Override
	public boolean equals(Object obj) {
		boolean ret = true;
		if (obj == null) {
			ret = false;
		}
		if (getClass() != obj.getClass()) {
			ret = false;
		}
		
		if (ret) {

			Equipo otroEquipo= (Equipo) obj;
			if(!nombreDelProyecto.equals(otroEquipo.getNombreDelProyecto())){
				return false;
			}
		}
		return true;
	}


}
